<?php



$to      = $email;
//$to = 'james@fconlinemarketing.com';

$subject = "Note: 24 Hours to Register a Friend.";

$message = "Hey" . "\n\n";

$message .= 'Quick note about your ticket to Martial ' . "\n" .
			'Arts Business Summit 2014. ' . "\n\n";

$message .= 'First off, we\'re excited you\'re going to ' . "\n" .
			'be joining us :-). I know this event is ' . "\n" .
			'going to be a big game changer for you, ' . "\n" .
			'and I applaud you for taking action and ' . "\n" .
			'registering. '  . "\n\n";

$message .= 'Second - the price recently went up by ' . "\n" .
			'$100 per ticket. This affects guest tickets ' . "\n" .
			'too. ' . "\n\n";

$message .= 'HOWEVER...' . "\n\n";

$message .= 'If you still want to bring another friend / guest, ' . "\n" .
			'for THE NEXT 24 HOURS, we are giving a ' . "\n" .
			'price break for all guest / friend tickets. '  . "\n\n";

$message .= 'You can bring someone else for just $149. ' . "\n\n";

$message .= 'But after 24 hours, the guest ticket price ' . "\n" .
			'goes up by $100. ' . "\n\n";

$message .= 'Sign your friends up now by clicking here:.' . "\n\n";

$message .= 'http://www.martialartsbusinesssummit2014.com/friend.php?uid=' . $md5 . "\n\n";

$message .= 'Peace :-)' . "\n" .
		 	'Mike Parrella' . "\n\n";


//$headers = 'From: info@martialartsbusinesssummit.com';
$headers = 'From: "info@fconlinemarketing.com" <info@fconlinemarketing.com>' . "\r\n" .
        	'Reply-To: info@fconlinemarketing.com' . "\r\n" .
			'BCC: printsupport@fconlinemarketing.com' . "\r\n" .
        	'X-Mailer: PHP/' . phpversion();

if(mail($to, $subject, $message, $headers)):

  $mail_result = 1;

else:
  $mail_result = 0;
endif;

?>