<?php

/* We will process the submitted form */
$mabs_product_id =  1;
$mabs_dvd_product_id = 2;

$mabs_product_cost = 0;
$mabs_dvd_product_cost = 0;
$guest_cost = 0;


date_default_timezone_set('America/New_York');

$order_date = date('Y-m-d h:i:s');



			# Email customer receipt
			$to      = $email;
			$subject = "Congrats, you're in! (IMPORTANT Info Enclosed!)";

				$message = "Hey" . "\n\n";

				$message .= "You're in. You've snagged one of the\nvery limited seats to Martial Arts\nBusiness Summit 2014." . "\n\n";

				$message .= "It's going to be a blast. And you're\ngoing to walk away with your mind\nabsolutely FRIED..." . "\n\n";

				$message .= "... with powerful, school-growing\ninformation." . "\n\n";

				$message .= "Meanwhile, it's time to get your flight\ntickets and book your hotel!" . "\n\n";

				$message .= "Here is your MABS14 Info Packet... " . "\n\n";

				$message .= "https://www.martialartsbusinesssummit.com/mabs-itenerary.pdf" . "\n\n";

				$message .= "Here is the hotel the event is\nlocated in:" . "\n\n";

				$message .= "http://www.marriott.com/hotels/travel/lgaap-new-york-laguardia-airport-marriott/" . "\n\n";

				$message .= "Mention MARTIAL ARTS BUSINESS\nSUMMIT for a special rate when you\ncall." . "\n\n";

				$message .= "-------------------------------------" . "\n\n";

				$message .= "Just to remind you, the dates\nare March 28 - 30, 2014." . "\n\n";

				$message .= "If you have any questions, don't\nhesitate to reply to this email\nor call us at (516) 543-0041." . "\n\n";

				$message .= "Also, between now and then,\nthink of any burning questions\nor problems you're facing in\nyour school." . "\n\n";

				$message .= "Think of questions you'd kill\nto know the answers to." . "\n\n";

				$message .= "You'll have opportunities\nto ask some of our industry's\nbrightest, most successful\nindividuals for help." . "\n\n";

				$message .= "I HIGHLY recommend taking\nadvantage of this." . "\n\n";

				$message .= "See you there. I can't wait." . "\n\n";

				$message .= 'Peace,' . "\n";
				$message .= 'MP' . "\n\n\n";

				$message .= "-------------------------------------" . "\n\n";

				$message .= "The details of your order are:" . "\n\n";

				$message .= "Order #:" . $order_product_id . "\n\n";

				$message .= "You have been charged: $" . number_format($order_total, 2) . "\n\n";

				$message .= "Which includes\n";

				$message .= "\t\t-1x MABS Ticket\n";

				if($guest):
					$message .= "\t\t-1x MABS Guest Ticket\n";
				endif;

				if($dvd):
					$message .= "\t\t-1x MABS DVD";
				endif;

				$message .= "\n\n-------------------------------------" . "\n\n\n";

				//$headers = 'From: info@martialartsbusinesssummit.com';
				$headers = 'From: "info@fconlinemarketing.com" <info@fconlinemarketing.com>' . "\r\n" .
            	'Reply-To: info@fconlinemarketing.com' . "\r\n" .
            	'X-Mailer: PHP/' . phpversion();

				mail($to, $subject, $message, $headers);


}

?>