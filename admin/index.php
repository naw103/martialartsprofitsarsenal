<?php

	require('program/program.php');

	date_default_timezone_set('America/New_York');

	$todays_date = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
	<title>Martial Arts Profits Arsenal Admin</title>

	<meta name="viewport" content="intial-scale=1.0" />

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<link rel="stylesheet" href="../css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/admin.css?ver=1.0" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


</head>
<body>
<?php

?>
<!-- BEGIN: Page Content -->
<div id="container">
	<div id="page_content">

		<?php include('header.php'); ?>

		<div id="opening_content"></div>

		<br /><br />

		<div id="featured_numbers">



			<!-- ticket sales -->
			<div id="ticket_sales" class="featured">
				<span class="head">Total</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($total, 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($orders); ?>)</span>
			</div>




			<!-- # orders today -->
			<div id="todays_orders" class="featured">
				<span class="head">Today's Orders</span><br />
				<span class="num"><?php echo $todays_orders; ?></span>
			</div>

		</div>


		<h2>All Orders</h2>

		<table class="main" style="width: 100%;">
			<thead>
				<tr class="head">
					<td>Order Id</td>
					<td>Name</td>
					<td>Phone</td>
					<td>Email</td>
					<td>Total</td>

					<td>
						Order<br />
						Date
					</td>
					<td>
						Hear About
					</td>
					<td>Status</td>
					<td>Added to Facebook</td>
					<!--<td>Payments</td>-->
				</tr>
			</thead>

			<tbody>

	   			<?php foreach($orders as $order): ?>
				<tr style="<?php echo $order['status'] != 'PAID' ? 'color: red;' : ''; ?>" data-orderid="<?php echo $order['order_id']; ?>">
					<td><?php echo $order['order_id']; ?></td>
					<td><?php echo ucwords(strtolower(stripslashes($order['firstname']))) . ' ' . ucwords(strtolower(stripslashes($order['lastname']))); ?></td>
					<td><?php echo $order['phone']; ?></td>

					<td><?php echo strtolower(trim($order['email'])); ?></td>

					<td><?php
							$d = $order['order_date'];
							if($order['order_total'] == 1 && strtotime(date('Y-m-d', strtotime("$d +1 month"))) < strtotime(date('Y-m-d'))):
								echo '$' . number_format(97, 2);
							else:
									echo '$' . number_format($order['order_total'], 2);
							endif;
					?>

					<td><?php echo date('m/d/y', strtotime($order['order_date'])); ?></td>

					<td>
						<?php
							$hear_about = $order['hear_about_us'];
							$txt = '';
							if($hear_about == 'Other'):
								$txt = 'Other: ' . stripslashes($order['hear_about_other']);
							elseif($hear_about == 'From a Friend'):
								$txt = 'Friend: ' . stripslashes($order['hear_about_friend']);
							else:
								$txt = $hear_about;
							endif;
						?>
						<?php echo $txt; ?>
					</td>

					<td><?php echo $order['status']; ?></td>

					<td><input type="checkbox" value="" name="added_facebook" class="added_facebook"  <?php echo $order['added_facebook'] == '1' ? 'checked="true"': ''; ?>/></td>

				</tr>

				<?php endforeach; ?>
		</tbody>

		</table>

		<?php if(isset($suspended_subscriptions)): ?>
		<div style="margin-top: 100px; border-top: 1px solid #CCC;">
			<h2 style="text-align: center; padding-top: 14px;">Suspended Subscriptions (<?php echo $t_suspended_subscriptions[0]['total']; ?> active members)</h2>

			<table>
				<thead>
					<tr>
						<!--<th style="text-align: center;">Order ID</th>-->
						<th style="text-align: center;">Name</th>
						<th style="text-align: center;">Email</th>
						<th style="text-align: center;">Phone</th>
						<th>State</th>
						<th style="text-align: center;">Date Suspended</th>
						<th style="">Payment Link</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($suspended_subscriptions as $suspended):
					   /*	$suspended['phone'] = str_replace('-', '', $suspended['phone']);
						$suspended['phone'] = str_replace('.', '', $suspended['phone']);
						$suspended['phone'] = str_replace(')', '', $suspended['phone']);
						$suspended['phone'] = str_replace('(', '', $suspended['phone']);
						$phone = substr($suspended['phone'], 0, 3) . '-' . substr($suspended['phone'], 3, 3) . '-' . substr($suspended['phone'], 6, 4);
						*/
					?>
					<tr <?php echo ($suspended['paid_status'] == 'CANCELLED') ? 'style="color: red;"' : ''; ?>>
						<!--<td><?php echo $suspended['order_id']; ?></td>-->
						<td><?php echo ucwords(strtolower($suspended['firstname'])) . ' ' . ucwords(strtolower($suspended['lastname'])); ?></td>
						<td style="text-align: left; padding-left: 10px;"><?php echo '<a href="mailto:' . strtolower(trim($suspended['email'])) . '>">' . strtolower(trim($suspended['email'])) . '</a>'; ?></td>
						<td><?php echo $suspended['phone']; ?></td>
						<td><?php echo $suspended['Code']; ?></td>
						<td><?php echo date('m/d/y', strtotime($suspended['date_suspended'])); ?></td>
						<td><a target="_blank" href="https://www.fconlinemarketing.com/billing/order/update/<?php echo $suspended['session_key']; ?>">View</a>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<?php endif; ?>



	</div>
</div>


<script src="js/jquery.dataTables.js"></script>


<script>
	$(document).ready(function() {
	    oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});


	  	$('table.main tbody').on('click', 'input.added_facebook', function () {

		status = '0';
		if($(this).is(':checked'))
		{
			status = '1';
		}

		var orderid = $(this).parents('tr').attr('data-orderid');
	   
		$.ajax({
				url: 'ajax/facebook.php',
				data: {
							status: status,
							orderid: orderid
				},
				type: 'POST'

		});

    	} );
		/*$('.added_facebook').click(function(){

		status = '0';
		if($(this).is(':checked'))
		{
			status = '1';
		}

		var orderid = $(this).parents('tr').attr('data-orderid');
		console.log(orderid);
		$.ajax({
				url: 'ajax/facebook.php',
				data: {
							status: status,
							orderid: orderid
				},
				type: 'POST'

		});

	});
	*/


	} );

	$(window).resize(function(){

		 jQuery(oTable).dataTable().fnDestroy();
         oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});

		var width = $(window).width();

		if(width > 1000)
		{
			oTable.width(1000);
		}
		else
		{
			o.Table.width(width);
		}
	});




</script>

</body>
</html>