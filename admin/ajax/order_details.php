<?php

if(isset($_POST)):

	$order_id = $_POST['order_id'];

	# Load database class
	require('../program/class.database.php');
	//require('functions.php');

	$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	$sql = "SELECT * 
			FROM checkout_orders co, checkout_customers cf 
			WHERE co.order_id = $order_id AND co.customer_id = cf.customer_id";

	$result = $db->Execute($sql);



	# Build XML
	header('Content-type: text/xml');
	header("Cache-Control: no-cache");
	echo "<?xml version=\"1.0\" ?>\n";
	echo "<response>\n";

		echo "\t<order_id>" . $_POST['order_id'] . "</order_id>\n";
		echo "\t<firstname>" . $result['firstname'] . "</firstname>\n";
		echo "\t<lastname>" . $result['lastname'] . "</lastname>\n";
		echo "\t<order_total>" . $result['order_total'] . "</order_total>\n";
		echo "\t<date_purchased>" . $result['date_purchased'] . "</date_purchased>\n";
	echo "</response>";

endif;

?>