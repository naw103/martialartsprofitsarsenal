<?php
	/*
	 * Script:    DataTables server-side script for PHP and MySQL
	 * Copyright: 2010 - Allan Jardine
	 * License:   GPL v2 or BSD (3-point)
	 */







# Load database class
require('../program/class.database.php');
//require('functions.php');

$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$db->open();

/*
	 * Paging
	*/
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}

// order details
$sql = "SELECT co.order_id, co.coupon_code, co.hear_about_us, cc.firstname, cc.lastname, co.order_date, co.order_total, co.subscription_type_id AS sub_id, cf.ID AS friend, cpj.ID AS dvd
		FROM checkout_orders co
			LEFT JOIN checkout_friends cf ON co.order_id = cf.order_id
			LEFT JOIN checkout_products_join cpj ON co.order_id = cpj.order_id AND cpj.product_id = 2,
			checkout_customers cc
		WHERE co.customer_id = cc.customer_id
		ORDER BY co.order_date DESC " . $sLimit;

$result = $db->Execute($sql);

$sql = "SELECT co.order_id FROM checkout_orders co
			LEFT JOIN checkout_friends cf ON co.order_id = cf.order_id
			LEFT JOIN checkout_products_join cpj ON co.order_id = cpj.order_id AND cpj.product_id = 2,
			checkout_customers cc
		WHERE co.customer_id = cc.customer_id
		ORDER BY co.order_date DESC";
$db->Execute($sql);
$maxResult = $db->getNumRows();

	$test['sEcho'] = intval($_GET['sEcho']);
	$test['iTotalRecords'] = $maxResult;
	$test['iTotalDisplayRecords'] = $maxResult;

	for($i = 0; $i < sizeof($result); $i++):

		$test['aaData'][] = array($result[$i]['firstname'] . '' . $result[$i]['lastname'], 'Y', 'Y', $result[$i]['order_total'], 'FULL', date('M d', strtotime($result[$i]['order_date'])), $result[$i]['hear_about_us'], '', '');

	endfor;

	//echo json_encode( $output );
	echo json_encode( $test );
?>
