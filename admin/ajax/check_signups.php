<?php
    require('../program/class.database.php');
	require('../program/functions.php');

	$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

    date_default_timezone_set('America/New_York');

    $todays_date = date('Y-m-d');

    //  orders today
    $tommorrow = date('Y-m-d', strtotime($todays_date . ' + 1 year'));

    $sql = "SELECT COUNT(*) as total
		FROM checkout_orders co
		WHERE DATE(order_date) >= DATE('$todays_date') AND DATE(order_date) < DATE('$tommorrow') AND order_product_id LIKE 'MABS-%'";


    $todays_orders = $db->Execute($sql);


    // total orders
  $sql = "SELECT COUNT(*) as total
  		FROM checkout_orders
		WHERE order_product_id LIKE 'MABS-%'";

  $total_orders = $db->Execute($sql);


  // # friends
$sql = "SELECT COUNT(*) as total
		FROM checkout_friends";

$total_friends = $db->Execute($sql);

// total dvd money
// total dvd money
$sql = "SELECT cpj.product_id, SUM(cp.cost) AS total
FROM checkout_products_join cpj, checkout_products cp, checkout_orders co
WHERE cpj.product_id = cp.product_id AND cpj.order_id = co.order_id AND co.coupon_code <> 'mastermind531'
GROUP BY cp.product_id";

$total_monies = $db->Execute($sql);

// order details
$sql = "SELECT co.coupon_code FROM checkout_orders co WHERE coupon_code = 'senseinick' || coupon_code = 'earlybird123' || coupon_code = 'earlybird321' || coupon_code = 'masterace' || coupon_code = 'godwin'";

$db->Execute($sql);

$coupon_discount = $db->getNumRows();

$total_monies[0]['total'] = $total_monies[0]['total'] - ($coupon_discount * 100);

// order details
$sql = "SELECT co.coupon_code FROM checkout_orders co WHERE coupon_code = 'mabshalf'";

$db->Execute($sql);

$coupon_discount1 = $db->getNumRows();

$total_monies[0]['total'] = $total_monies[0]['total'] - ($coupon_discount1 * 200);

$ticket_sales = $total_monies[0]['total'] + $total_monies[2]['total'] + $total_monies[3]['total'] + $total_monies[4]['total'] + $total_monies[5]['total'];

$dvd_sales = $total_monies[1]['total'];

$overall_total = $ticket_sales + $dvd_sales;


// order details
$sql = "SELECT co.order_id, co.coupon_code, cc.firstname, cc.lastname, co.order_date, co.order_total, co.subscription_type_id AS sub_id, cf.ID AS friend, cpj.ID AS dvd
		FROM checkout_orders co
			LEFT JOIN checkout_friends cf ON co.order_id = cf.order_id
			LEFT JOIN checkout_products_join cpj ON co.order_id = cpj.order_id AND cpj.product_id = 2,
			checkout_customers cc
		WHERE co.customer_id = cc.customer_id
		ORDER BY co.order_date DESC";

$order_details = $db->Execute($sql);



// mastermind order details
$sql = "SELECT co.customer_id
		FROM checkout_orders co
			LEFT JOIN checkout_friends cf ON co.order_id = cf.order_id
			LEFT JOIN checkout_products_join cpj ON co.order_id = cpj.order_id AND cpj.product_id = 2,
			checkout_customers cc
		WHERE co.customer_id = cc.customer_id AND order_product_id LIKE 'MABSMASTERMIND%'
		ORDER BY co.order_date DESC";

$mastermind = $db->Execute($sql);




    $db->close();

  	# Build XML
	header('Content-type: text/xml');
	header('Cache-Control: no-cache');
	echo "<?xml version=\"1.0\" ?>\n";
	echo "<response>\n";

		echo "\t<todays_orders>" . $todays_orders['total'] . "</todays_orders>\n";
        echo "\t<total_orders>" . $total_orders['total'] . "</total_orders>\n";
        echo "\t<total_friends>" . $total_friends['total'] . "</total_friends>\n";

		echo "\t<ticket_sales>" . number_format($ticket_sales) . "</ticket_sales>\n";
       	echo "\t<dvd_sales>" . number_format($dvd_sales) . "</dvd_sales>\n";
        echo "\t<total_monies>" . number_format($overall_total + (sizeof($mastermind) * 297)) . "</total_monies>\n";

		echo "\t<mastermind>" . sizeof($mastermind) . "</mastermind>\n";

		for($i = 0; $i < sizeof($order_details); $i++):
	    echo "\t<customer>\n";

			echo "\t\t<customer_name>" . $order_details[$i]['firstname'] . ' ' . $order_details[$i]['lastname'] . "</customer_name>\n";
			echo "\t\t<friend>" . (is_null($order_details[$i]['friend']) ? 0 : 1) . "</friend>\n";
			echo "\t\t<dvd>" . (is_null($order_details[$i]['dvd']) ? 0 : 1) . "</dvd>\n";



		echo "\t</customer>\n";

		endfor;



	echo "</response>";

?>