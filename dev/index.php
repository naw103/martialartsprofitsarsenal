<?php



	require('checkout/program/program.php');
	require('checkout/program/definitions.php');
	require('checkout/program/class.checkout.php');
	require('checkout/program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

     $all_orders = $db->all_orders($site_id);
	$class = '';
    if(sizeof($all_orders) >= 255):
        $db->price_change();
		$class = 'above_250';
    endif;

	$mapa = $db->get_product_info($mapa_id, fALSE);

	$states = $db->get_all_states();
	$countries = $db->get_all_countries();

	$mapa_cost = (int)$mapa['ez_pay'][0]['cost'];
	$ezpay_id = $mapa['ez_pay'][0]['id'];
	$ezpay_num = $mapa['ez_pay'][0]['num'];
	$ezpay_interval = $mapa['ez_pay'][0]['interval'];
	$total_purchased = $mapa_cost;
	$start_total = $mapa_cost;



	$db->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<title>Martial Arts Profits Arsenal by Mike Parrella</title>

	<meta property="og:title" content="Martial Arts Profits Arsenal by Mike Parrella" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="https://www.martialartsprofitsarsenal.com/images/mapa.jpg" />
	<meta property="og:url" content="https://www.martialartsprofitsarsenal.com/" />
	<meta property="og:description" content='MA Profits Arsenal: A "Done for You" Stash of Incredible Marketing Content to Grow Your School, FAST. Now Live! Click now to learn more and get started today!' />
	<meta property="og:site_name" content="martialartsprofitsarsenal.com/" />

	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/ekko-lightbox.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="early/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	  <script src="js/html5shiv.js"></script>
	  <script src="js/respond.min.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.png">
	<link rel="stylesheet" href="css/simple-line-icons.css"/>
	<link rel="stylesheet" href="css/responsive.css"/>

</head>

<body class="<?php echo $class; ?>">
	<div id="preloader">
		<div id="status"></div>
	</div>
    <!-- ******************** MASTHEAD SECTION ******************** -->

	<!-- STICKY NAVIGATION -->
	<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
		<div class="container">
			<div class="navbar-header">

				<!-- LOGO ON STICKY NAV BAR -->
				<button type="button" class="navbar-toggle" data-toggle="" data-target="#kane-navigation">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#"><img src="images/bullet.png" alt=""></a>

			</div>

			<!-- NAVIGATION LINKS -->
			<div class="navbar-collapse collapse" id="kane-navigation">
				<ul class="nav navbar-nav navbar-right main-navigation">
					<!--<li><a href="#home">Home</a></li>-->
					<li><a href="#explore">What it is</a></li>
					<li><a href="#what_you_get">What you get</a></li>
					<li><a href="#reviews">Rave Reviews</a></li>
					<li><a href="#faq">FAQ</a></li>
					<!--<li><a href="#location">Sticky Header</a></li>-->
                    <li><a href="#prices">Prices &amp; Guarantee</a></li>
					<li class="tickets"><a href="checkout/index.php">Get Started!</a></li>
				</ul>
			</div>
		</div> <!-- /END CONTAINER -->
	</div> <!-- /END STICKY NAVIGATION -->

	<main id="top" class="masthead" role="main">


<div id="top_part">
		<div id="top_part_container">
            <div><i class="fa fa-warning"></i></div>
            <div id="price_txt">Price raises to $97 / month after 250 sign ups! Hurry and buy now!</div>
            <div id="close_top">X</div>
        </div>
</div>
		<div class="container">


				<h2 class="w smaller">Mike Parrella's <span class="bullet white"></span> Martial Arts Profits Arsenal</h2> <br>
            <h3 class="w x">An Ever-Growing "Stash" of Strategies, Tactics, &amp; Done-for-You Marketing to Grow Your School, FAST.</h3>

            <h4 class="w z">Thousands of dollars of <b>income-generating</b> content. All for just <b>$<?php echo $mapa_cost; ?></b> / month.</h4><br>
			<br>
			<a href="https://www.martialartsprofitsarsenal.com/checkout/index.php">
			  	 <button class="btn btn-warning btn-lg">Gain Instant Access!</button>
			</a>



		</div><!--/container -->
	</main><!--/main -->


    <!-- ******************** FEATURES SECTION ******************** -->
	<div class="container explore1" id="explore">



		<div class="section-title">

			<h2 class="max-width border-bottom">What is the <span class="bullet black"></span> Martial Arts Profits Arsenal?</h2>
		</div>

		<section class="mine">
            <img src="images/2-mike.png" width="" align="left" hspace="50">
			<p class="big"><em>Hey, Mike Parrella here, and here's the deal:</em></p>
            <p>First off, if you're not so familiar with me, here are the cliff notes...</p>
            <ul>
                <li> I run 5 super successful schools. Each produces multiple-6-figure profits, and two have recently broken the $100,000 per MONTH mark.</li>
                <li>My companies FC Online Marketing & iLoveKickboxing have revolutionized online marketing for martial arts. They have literally produced hundreds of thousands of PAID intros for schools in the last few years alone.</li>
                <li>I'm passionate about helping schools break free from the tired, old tactics that simply don't work. I'm here to show you a new, profitable, FUN way to do things.</li>
            </ul>
            <p>Recently, I launched MA Business Accelerator to help even more schools grow and flourish. At $199 / month, it opened my coaching up to way more schools than my previous coaching program (which goes for $18,500 / year).</p>
            <p>But I've realized something: There are STILL schools who are stuck in such a rut, even $199 / month is out of the question. So I hit the drawing board, and came up with another solution.</p>
            <p>See... every month, my companies produce ground-breaking martial arts marketing materials. This includes <b class="redder">Facebook ads &amp; strategies, emails that convert prospects into students, ninja SEO tactics, referral secrets, retention secrets, and more.</b></p>
            <p><em>The MA Profits Library is a monthly membership that gives you access to all of the above for <b>just $<?php echo $mapa_cost; ?> / month.</b> Every month, as my team and I create new tactics and materials, we send them to you.</em></p>
            <p>Sounds good? Then hop on board now. Because at this low of a price, and this high of a value, I don't know how long I'll keep this out there.</p>
            <p class="big"><em>Peace,</em><br>
            <img src="images/signature.png"></p>
			</section>
		</div>
    <section class="desc padding-zero">

    <div class="container explore2" id="explore">

		<div class="section-title">
			<h2 class="max-width border-bottom">The Team Parrella Assassins Behind Profits Arsenal</h2>
		</div>

		<section class="mine2">

                 <div class="col-md-1"><img src="images/75.png"></div>
            <div class="col-md-2">
            <img src="images/ryan.jpg">
                <p class="text-center">Ryan Healy<br>
                <em class="top-bottom">Special Forces</em></p>
            </div>
            <div class="col-md-2">
            <img src="images/nick.jpg">
                <p class="text-center">Nick Dougherty<br>
               <em class="top-bottom">Artillery</em></p>
            </div>
            <div class="col-md-2">
            <img src="images/david.jpg">
                <p class="text-center">David Tendrich<br>
                <em class="top-bottom">Hotshot Sniper</em></p>
            </div>
            <div class="col-md-2">
            <img src="images/kelly.jpg">
                <p class="text-center">Kelly Murray<br>
                <em class="top-bottom">Demolitions</em></p>
            </div>
            <div class="col-md-2">
            <img src="images/adan.jpg">
                <p class="text-center">Adan de la Cruz<br>
                <em class="top-bottom">"The Tank"</em></p>
            </div>
             <div class="col-md-1"><img src="images/75.png"></div>
			</section>
		</div>
    </section>
		 <!--/section -->

    <section id="what_you_get" class="padding">
  <div class="container">
		<div class="row text-center">

			<h2 class="max-width border-bottom">Here's exactly what you'll get...</h2>

            <br><br>
		 			 </div>
      <div style="max-width:85%;margin:0 auto;">
      <div class="col-md-6">
      <h4>Student-Getting Facebook Ads</h4>

			<p>Our ads have been TESTED and PROVEN to bring in new students. Not only that - we'll give you our top-secret strategies for targeting your ads and following up with Facebook leads.</p>
          <p>We give you the graphics, the writing, and everything else. You just copy, paste, and deploy.</p>
           </div>
      <div class="col-md-6">
          <i class="icon-social-facebook p"></i>
     <!-- <img class="space" src="images/fb.png">-->
      </div>

      <div style="clear:both;"></div>
      <br><br>
      <div class="col-md-6">
      <i class="icon-envelope p"></i>
      </div>
      <div class="col-md-6">
      <h4>High-Converting Emails</h4>

			<p>If you're like most schools - your email marketing needs help... desperately! Not to worry. We've got just the answer.</p>
          <p>In Profits Arsenal, you receive pre-written emails EVERY month that include amazing sales pitches (that are friendly, and not "salesy" or "pressurey")... as well as awesome, inspiring content that provokes gratitude and action from your list.</p>
           </div>

          <div style="clear:both;"></div>
      <br><br>


          <div class="col-md-6">
      <h4>The Secret to Constant Referrals</h4>

			<p>One of the things we're known best for is helping schools create a "culture" of referrals. That's where referring people to your school is as natural as breathing.</p>
          <p>You'll receive powerful tips every month that help you do the same, so you too can turn your school into a well-oiled, referral-generating machine.</p>
           </div>
      <div class="col-md-6">
      <i class="icon-graph p"></i>
      </div>

          <div style="clear:both;"></div>
      <br><br>

        <div class="col-md-6">
      <i class="icon-heart p"></i>
      </div>
      <div class="col-md-6">
      <h4>Hiring, Training, &amp; Managing a Staff of School-Growing Rockstars.</h4>

			<p>Mike Parrella's 5 schools have over 30 employees. Those employees are all trained, managed, and motivated by the great Kelly Murray. And they're the most driven, enthusiastic bunch you'll ever find.</p>
          <p>Each month, Kelly will give you tips that will solve every problem you've ever faced with staff. She'll teach you how to inspire your team to "take ownership" over your school so they treat it like its their own. That means they work hard every day to not just teach great classes... but to help you grow.</p>
           </div>

          <div style="clear:both;"></div>
      <br><br>


          <div class="col-md-6">
      <h4>The Most Hardcore, Ninja SEO Secrets You'll Ever Encounter.</h4>

			<p>Adan De La Cruz has forgotten more about SEO for martial arts than just about any other human being on the planet will ever learn. He's the reason FCOM and iLoveKickboxing's webites completely DOMINATE the search engines for their markets.</p>
          <p>Adan is going to pry open his massive brain and give you the knowledge you need to dominate in your town (and surrounding towns) in the searches.</p>
           </div>
      <div class="col-md-6">
     <i class="icon-chemistry p"></i>
      </div>

          <div style="clear:both;"></div>
      <br><br>

          <div class="col-md-6">
      <i class="icon-energy p"></i>
      </div>
      <div class="col-md-6">
      <h4>And more.</h4>

			<p>We're going to give you done-for-you content to help you grow, and advice that will transform your school into an unstoppable brand.</p>
          <p>You'll also get to interact with your Featured Experts as well as your fellow school owners in our Secret Facebook Group for Profits Arsenal. It's the best $<?php echo $mapa_cost; ?> you'll ever spend.</p>
           </div>

          <div style="clear:both;"></div>
      <br><br>


    </div>


				</div>

</section>
    <section>


		<div id="reviews" class="highlight testimonials desc">
			<div class="container">
			<br>
			<h2 class="max-width border-bottom text-align">School Owners Worldwide Speak Out</h2>
                <br>

					<div class="col-md-12 margin-bottom">
                        <center>
                            <h5 style="max-width:700px;margin:0 auto;padding:0 0 30px;">Watch this 2 minute video to hear real school owners share their authentic experiences and results.</h5>
						<!--<iframe width="640" height="360" src="//www.youtube.com/embed/iBvk4dwVCtE?rel=0" frameborder="0" allowfullscreen></iframe></center>-->

					</div><!--/col-md-6 -->
                       	<iframe  id="testimonial_video" src="//www.youtube.com/embed/IUeFS2WaIV0?rel=0" frameborder="0" allowfullscreen></iframe>




		</div><!-- row -->
        <br><br>
        <div class="container ">
            <h4 class="center">Even more rave reviews...</h4>
            <br><br>
            <div class="col-md-6 center">
                <img src="/images/testimonial-images/both-shane-tassoul.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-andy-wilson.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-ben-moriniere.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-brently-bazor.png" alt="" />
                <img src="/images/testimonial-images/mapa-jason-landaas.jpg" alt="" />


                <img src="/images/testimonial-images/mapa-jen-degnan.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-joe-gregory.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-john-geyston.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-manny-cabrera.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-melody-shuman.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-tyron-clark.png" alt="" />
            </div>
            <div class="col-md-6 center">
                <img src="/images/testimonial-images/both-vincent-duchetta.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-daniel-miller.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-david-jenne.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-eric-meyers.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-hayes-freeman-kim-brewster.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-jamal-al.jpg" alt="" />

                <img src="/images/testimonial-images/mapa-john-geyston-2.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-josh-deane.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-krystine-jones-don-duncan.png" alt="" />
                <img src="/images/testimonial-images/mapa-mario-cesario-2.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-mike-evans.png" alt="" />
                <img src="/images/testimonial-images/mapa-troy-miller.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-troy-miller-2.jpg" alt="" />
                <img src="/images/testimonial-images/mapa-vincent-duchetta.jpg" alt="" />
            </div>
        </div>
   <!--	</section>-->
		</div>

		</section><!--/section -->


 <section id="faq" class="margin-top padding">
		<div class="container">
			<h2 class="max-width border-bottom text-align">FAQ</h2>

            <h3 style="text-align: center; padding-bottom: 20px; font-size: 35px;">Click a question to reveal its answer!</h3>

			<div class="row">
				<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">Who are Mike Parrella and his team of experts, and how are they qualified to help my school grow?</div>
							<div id="question1" class="">
								<div class="panel-body">
									Mike Parrella is the world-renowned, multi-school owner who transformed online marketing for martial arts. He pioneered an online paid enrollment system that has literally turned the industry on its head.<br><br>His companies, FC Online Marketing and iLoveKickboxing, have generated hundreds of thousands of PAID leads for schools in the past couple of years alone. The experts in Profits Arsenal are his all-star team who make magic happen. There's no one better qualified on the planet to help your school grow.
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">What's the cost? Do you offer a guarantee?</div>
							<div id="question2" class="">
								<div class="panel-body">
									Profits Arsenal costs just $<?php echo $mapa_cost; ?> / month. Cancel any time. We're so confident in the quality of our content that we even offer a 100% Money Back Guarantee on your first 30 days. The terms are simple: You exceed your investment, or we give you a refund.
								</div>
							</div>
						</div>

				</div>
				<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">What exactly do I get when I join?</div>
							<div id="question4" class="">
								<div class="panel-body">
									Every week, you'll receive powerful tips (in the form of articles and videos) as well as done-for-you content. This includes advice on staffing, growth, referrals, retention, marketing, and more. And it includes content such as done-for-you Facebook ads for BOTH kids and adults as well as done-for-you emails that contain powerful content and effective sales pitches. You'll also get access to our Secret Facebook Group where you can interact with the experts behind Profits Arsenal as well as your fellow school owners.
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">How do I sign up now?</div>
							<div id="question5" class="">
								<div class="panel-body">
									Just <b><a href="https://www.martialartsprofitsarsenal.com/checkout/index.php">click here</a></b> to head to the check out page now! Complete your order and you'll receive access to the members area within 1 business day.
								</div>
							</div>
						</div>

				</div>
			</div>
		</div>
	</section>
	<!-- FAQ END-->




	    <!-- ******************** PRICING LIST ******************** -->
<main class="special-bg">
<div id="prices"  class="container">

			<div class="section-title">

				<h2 class="w max-width border-bottom-w">Price &amp; Guarantee
</h2>

			</div>
			<section class="row breath planpricing">

				<div class="col-md-7">
					<div class="pricing color2">

						<div class="planname">MA Profits Library</div>
						<div class="price"> <span class="curr">$</span><?php echo $mapa_cost; ?> / mo.<span class="per"></span></div>


                        <div class="billing" style="text-align:center;"><b>Every month you'll get:</b></div> <br>
						<div class="billing"><span class="bullet red"></span> Up to 4 (or more!) High-Converting Facebook Ads! ($150 value)</div>
						<div class="billing"><span class="bullet red"></span> Up to 4 (or more!) Killer Emails! ($100 value)</div>
                        <div class="billing"><span class="bullet red"></span> 16+ Powerful Lessons on School-Growth! On staffing, referrals, retention, and getting more new students in the door. ($400 value)</div>
						<div class="billing"><span class="bullet red"></span> Plus Secret Bonuses! ($100's value)</div>

					</div><!--/pricing -->
				</div>

				<div class="col-md-5">
					<div class="pricing color3">
						<div class="planname w border-bottom-w">30-Day 100% Money Back Guarantee</div><br>

						<p class="w text-left" style="margin-bottom:5px;">Try Profits Arsenal out for 30 days. If you're unhappy or don't return a profit on your investment in that time - tell us for a full refund. Hassle-free: The way it should be.<br><br>Fair enough?</p><br><br>
                        <i class="icon-star"></i>
                        <br><br>
                        </div>
				</div><!--/col-md-4-->



                <div class="col-md-12 text-center padding">
				<a href="https://www.martialartsprofitsarsenal.com/checkout/index.php">
                <button class="btn btn-warning btn-lg">Register Now!</button>
				</a>
                </div>


			</section><!-- /section planpricing -->
 </div>
</main>


	    <!-- ******************** FOOTER ******************** -->


		<div class="container">
			<section class="row breath">
				<div class="col-md-12 footerlinks">
					<p>&copy; 2014 Parrella Consulting Inc. All Rights Reserved</p>
				</div>
			</section><!--/section -->
		</div><!--/container -->


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
        <h3 style="text-transform:none !important;font-weight:bold !important;">Coming soon! Officially opens on Monday, October 6, 2014!</h3>
          <h4 style="font-weight:normal !important;text-transform:none !important;"><a href="http://profitsarsenal.com/early/index.html">Click here</a> for some FREE school-growing videos while you wait!</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




 <!--/javascript -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/easing.js"></script>
<!--<script src="js/nicescroll.js"></script>-->
<script src="js/lightbox.min.js"></script>
<script src="js/jquery.nav.js"></script>

<script>
 $(window).load(function() {
	//Preloader
	$('#status').delay(300).fadeOut();
	$('#preloader').delay(300).fadeOut('slow');
    //$(".navbar").css('height','0px');
	$('body').delay(550).css({'overflow':'visible'});
    $('#top_part').delay(550).fadeIn('500');
})
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

 $(function() {
    $('.scrollto, .gototop').bind('click',function(event){
		 var $anchor = $(this);
		 $('html, body').stop().animate({
         scrollTop: $($anchor.attr('href')).offset().top
          }, 1500,'easeInOutExpo');
     event.preventDefault();
      });
  });
  $(window).load(function(){
    $('.preloader').fadeOut(3000); // set duration in brackets

});

</script>

    <script>
    // set the date we're counting down to
    /*
var target_date = new Date('Jan, 31, 2014').getTime();

// variables for time units
var days, hours, minutes, seconds;

// get tag element
var countdown = document.getElementById('countdown');

// update the tag with id "countdown" every 1 second
setInterval(function () {

    // find the amount of "seconds" between now and target
    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;

    // do some time calculations
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    // format countdown string + set tag value
    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
    + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>';

}, 1000);
*/
</script>


<script>


var elem = $('#faq .panel');

elem.find('.panel-heading').each(function(){



});

</script>

<script>
    $(document).ready(function(){

        $('#close_top').click(function(){
           $('#top').css('margin-top', '0px');
           $('#top_part').fadeOut(600);
        });

$(".navbar-toggle").click(function(){
    var element = $(this).attr('data-target');
    if($(element).hasClass('collapse')){
        $(element).removeClass('collapse');
    }
    else
    {
        $(element).addClass('collapse');
    }
});


/* =================================
===  STICKY NAV                 ====
=================================== */

$(document).ready(function() {
  $('.main-navigation').onePageNav({
    scrollThreshold: 0.2, // Adjust if Navigation highlights too early or too late
    filter: ':not(.external)',
    changeHash: true
  });

});


/* COLLAPSE NAVIGATION ON MOBILE AFTER CLICKING ON LINK - ADDED ON V1.5*/

if (matchMedia('(max-width: 480px)').matches) {
    $('.main-navigation a').on('click', function () {
        $(".navbar-toggle").click();
    });
}


/* NAVIGATION VISIBLE ON SCROLL */

$(document).ready(function () {
    mainNav();
});

$(window).scroll(function () {
    mainNav();
});

if (matchMedia('(min-width: 992px), (max-width: 767px)').matches) {
  function mainNav() {
        var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        if (top > 40) $('.sticky-navigation').stop().animate({"top": '0'});

        else $('.sticky-navigation').stop().animate({"top": '-60'});
    }
}

if (matchMedia('(min-width: 768px) and (max-width: 991px)').matches) {
  function mainNav() {
        var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        if (top > 40) $('.sticky-navigation').stop().animate({"top": '0'});

        else $('.sticky-navigation').stop().animate({"top": '0'});
    }
}


    });
</script>

</body>
</html>