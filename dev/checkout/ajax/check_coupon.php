<?php

if(isset($_POST))
{
	$coupon_code = strtolower(trim($_POST['coupon_code']));

	include('../program/program.php');
	include('../program/definitions.php');
	include('../program/class.checkout.php');

    $coupon = array();
    date_default_timezone_set('America/New_York');

	$today = date('Y-m-d H:i:s');
	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

    $query = "SELECT
						*
			  FROM
			  			checkout_coupons cc
			  WHERE
			  			DATE(start_date) <= DATE('$today') AND
						DATE(end_date) >= DATE('$today') AND
						coupon_code = '$coupon_code' AND
						site_id = '$site_id' AND
						active = 1";
    
	$coupon = $db->Execute($query);

	if($db->getNumRows() > 0):
        // active coupon

        $data['status'] = 1;
        $data['msg'] = 'Coupon has been successfully applied';
        $data['amount'] = $coupon['amount'];
		$data['free'] = $coupon['FREE'];
        $data['type'] = $coupon['type'];
        $data['products'] = $coupon['products'];
	else:
        // no active coupon
		$data['status'] = 0;
        $data['msg'] = 'Invalid Coupon';
        $data['amount'] = '';
        $data['type'] = '';
		$data['free'] = '';
        $data['products'] = '';
	endif;

    //$data['products'] = 3;
    # return xml

    header('Content-type: text/xml');
    header('Cache-control: no-cache');

    echo "<?xml version=\"1.0\" ?>\n";
    echo "<response>\n";
    echo "\t<coupon>\n";

    echo "\t\t<status>" . $data['status'] . "</status>\n";
    echo "\t\t<msg>" . $data['msg'] . "</msg>\n";
    echo "\t\t<amount>" . $data['amount'] . "</amount>\n";
    echo "\t\t<type>" . $data['type'] . "</type>\n";
    echo "\t\t<products>" . $data['products'] . "</products>\n";

    echo "\t</coupon>\n";
    echo "</response>";

}

?>