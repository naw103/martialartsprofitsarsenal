<?php

if(isset($_POST))
{

    require('../program/definitions.php');
	require('../program/class.checkout.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	$db->open();

	$country_id = $_POST['country_id'];

	$states = $db->get_all_states($country_id);

	$db->close();


	# Build XML
	header('Content-type: text/xml');
	echo "<?xml version=\"1.0\" ?>\n";
	echo "<response>\n";

	for($i = 0; $i < sizeof($states); $i++):

		echo "\t<state>\n";
			echo "\t\t<state_id>" . $states[$i]['ID'] . "</state_id>\n";
			echo "\t\t<state_name>" . $states[$i]['State'] . "</state_name>\n";
		echo "\t</state>\n";

	endfor;


	echo "</response>";

}


?>