<?php

# CRON JOB: Run this process every minute to ensure that all receipt emails are sent.
#
# NEED: It has been determined that there are collisions when checking out, and due to
#       this we decided to remove the email process from the checkout.
#
# AUTHOR:   Scott Gray
# CREATED:  4/10/2014 09:35

# Include the required files.

#/var/chroot/home/content/41/6698841/html/ilovekickboxing.com/checkout/ajax_program/error.php

require("/home/content/65/11672465/html/martialarts100kmastermind.com/1day/program/database.php");
require_once('/home/content/65/11672465/html/martialarts100kmastermind.com/1day/program/swiftmail/swift_required.php');

global $email_failed;
global $sending_address;
global $developer;

$developer = "printsupport@fconlinemarketing.com";

error_reporting(E_ALL);

try{


$total_emails_sent = email_receipt();
mail("printsupport@fconlinemarketing.com","Cron Complete","Process complete success.\n\n\nReceipts sent: $total_emails_sent");
}
catch(Exception $e) {
    # Log the fact that there was an error.
    //echo "Logging error.<br>";
    echo "<pre>";
    //print_r($e);
    echo "</pre>";
    try{
       $estatus = handle_exception($e);
    }
    catch(Exception $ee){
        mail('printsupport@fconlinemarketing.com','Receipt Error',$e);
    }
}

try{


$total_emails_sent = email_receipt_mabsdvd();
mail("printsupport@fconlinemarketing.com","Cron Complete","Process complete success.\n\n\nReceipts sent: $total_emails_sent");
}
catch(Exception $e) {
    # Log the fact that there was an error.
    //echo "Logging error.<br>";
    echo "<pre>";
    //print_r($e);
    echo "</pre>";
    try{
       $estatus = handle_exception($e);
    }
    catch(Exception $ee){
        mail('printsupport@fconlinemarketing.com','Receipt Error',$e);
    }
}


function email_receipt()
{
        # This will send the email using the SwiftMailer library.
        # This is a major email revision to the checkout for iLoveKickboxing.com.
        # This method addresses the problem of hitting the email limit on the server,
        # it also resolves the spam index issue that we are experiencing when sending the
        # receipts and registrations.

        global $connection;
        global $table_customers;
        global $table_signups;
        global $price;
        global $email_failed;
        global $sending_account;
        global $developer;

        $process_total = 0;

        # Connect to the database.
        dbConn();

        # Housekeeping procedure: In the event that the date in the database is
        # before today, we want to set the relays used to 0.
        $today = date("Y-m-d");
        $sql = "UPDATE ckt_email_credentials SET smtp_relays_used = 0, smtp_relay_date = '$today' WHERE smtp_relay_date < '$today';";

        # Perform the query
        mysql_query($sql);


        # Query the database for the mail account to use for this email.
        $sql = "SELECT * FROM ckt_email_credentials ORDER BY id DESC;";

        # Get the result set.
        $account_result = mysql_query($sql);

        # Determine the mail account to use for this, we want to ensure that we do not
        # move over the 250 SMTP relays for a given account.
        while($account = mysql_fetch_object($account_result)):
            if($account->smtp_relays_used < 230):
                $smtp_credentials = $account;
            endif;
        endwhile;

        $sending_account = $smtp_credentials->username;

        ###### START: Debug Block ######
        #echo "<pre>";
        #print_r($smtp_credentials);
        #echo "</pre>";
        ###### END: Debug Block ######

        # Create the SMTP transporter.
        $transport = Swift_SmtpTransport::newInstance($smtp_credentials->smtp_server_information, $smtp_credentials->smtp_port)
          ->setUsername($smtp_credentials->username . "@fconlinemarketing.com")
          ->setPassword($smtp_credentials->password)
          ;

        # Create the mailer, use the above created transport.
        $mailer = Swift_Mailer::newInstance($transport);

        # We need to get some information on each of the signups that need receipts.
        $sql = "SELECT
                			cc.customer_id AS id,
							cc.firstname,
                			cc.lastname,
                			cc.email,
                			co.order_total,
                			co.cc_type,
                            co.cc_num,
                			co.cc_exp_month,
                			co.cc_exp_year,
                			co.order_date,
                			co.order_product_id
                FROM
                			checkout_orders co,
                			checkout_customers cc
                WHERE
                			cc.customer_id = co.customer_id AND
                			co.order_product_id LIKE '1DAYMM-%' AND
                			co.receipt_sent = '0'";

        $send_result = mysql_query($sql);

        while($row = mysql_fetch_assoc($send_result)):
        # Set some housekeeping variables.
        $emails_result = 0;
        $email_counter = 0;
        $total_relays = 0;


        //$location = $row['franchise_id'];
        $invoice_num = $row['order_product_id'];


        # Get the school data from the schools table for this school.
        //$school_data = get_location_payment($location);


        #################################################################################
        # START: Exception Handler
        #################################################################################
        # In the even this school does not have an email associated with it.
        # This causes an exception will will cause email not to be sent.
      //  if($school_data['franchise_email'] == ''):
            # Set it to email to signups instead...
       //     $school_data['franchise_email'] = "signups@ilovekickboxing.com";

            # Notify the developer that there is a missing email.
       //     mail("$developer","ERROR: Missing Franchise Email","{$school_data['Name']} with an ID of {$row['franchise_id']}\n Does not have an email address associated wth it.");
       // endif;
        #################################################################################
        # END: Exception Handler
        #################################################################################


        # Get the customer data from the  checkout tables for this customer.
       // $sql = "SELECT cc.*, cs.* FROM ckt_customers cc, ckt_signups cs WHERE cs.customer_id = cc.customer_id AND cc.id = $client_id";
	           $sql = "SELECT
                			cc.customer_id,
							cc.firstname AS first_name,
                			cc.lastname AS last_name,
                			cc.email,
                            cc.phone,
                            cc.address,
                            cc.city,
                            cc.zipcode,
							co.subscription_type_id,
                			co.order_total,
                			co.cc_type,
                            co.cc_num,
                			co.cc_exp_month,
                			co.cc_exp_year,
                			co.order_date,
                			co.order_product_id,
							co.mm_option,
							s.Code,
							c.CountryCode,
                            co.coupon_code,
                            mm.ID AS mm_id,
                            mm.Date
                FROM
                			checkout_orders co,
                			checkout_customers cc,
                			Countries c,
                			States s,
                			mm_1day_options mm
                WHERE
                			cc.state_id = s.ID AND
                			cc.country_id = c.ID AND
                			cc.customer_id = co.customer_id AND
                			mm.ID = co.mm_option AND
                			co.order_product_id = '$invoice_num'";

        $customer_result = mysql_query($sql);



        while($row = mysql_fetch_assoc($customer_result)):

	        $client_name = $row['first_name'] . " " . $row['last_name'];
            $client_first = $row['first_name'];
            $client_last = $row['last_name'];
            //$client_address1 = $row['address1'];
            //$client_address2 = $row['address2'];
            //$client_city = $row['city'];
            //$client_state = $row['state'];
            //$client_zip = $row['zip'];
            $customer_id = $row['customer_id'];
            $phone = $row['phone'];
            $address = $row['address'];
            $city = $row['city'];
            $state = $row['Code'];
            $country = $row['CountryCode'];
            $email = $row['email'];
            $zipcode = $row['zipcode'];
            $coupon_code = $row['coupon_code'];
            $order_id = $row['order_product_id'];

			if((int)$row['subscription_type_id'] == 7):
				$order_total = '$' . number_format($row['order_total'], 2) . ' (3 Monthly Payments of $99)';
			else:
            	$order_total = '$' . number_format($row['order_total'], 2);
			endif;

			$mm_text = stripslashes($row['Date']);
            //$transaction_id = $row['transaction_id'];
            //$auth_code = $row['approval_code'];
            $order_date = $row['order_date'];
            $order_date = date('m/d/y h:i a', strtotime($order_date));
            //$post_date = $row['post_date'];
            $cc_num = $row['cc_num'];
            $card_type = $row['cc_type'];
            $cc_exp = $row['cc_exp_month'] . '/' . $row['cc_exp_year'];
           // $deal_data = getDeal($row['web_special']);
           // $phone_area_code = substr($school_data['phone'],0,3);
           // $phone_prefix = substr($school_data['phone'],3,3);
           // $phone_suffix = substr($school_data['phone'],6,4);
           // $school_phone = "(" . $phone_area_code . ") " . $phone_prefix . "-" . $phone_suffix;
            //$home_phone = $row['home_phone'];
            //$mobile_phone = $row['mobile_phone'];
           // $hear_about = $row['hear_about'];
           // $hear_about_other = $row['hear_about_other'];
           // $hear_about_friend = $row['friend_name'];
        endwhile;

        # Get Friend Signup Data.
        //$sql = "SELECT cf.* FROM ckt_friend_signup cf WHERE order_id = '$order_id';";
        //echo $sql; die;
        //$friend_result = mysql_query($sql);

       // if($friend_result):
       //     while($row = mysql_fetch_assoc($friend_result)):
       //         $friend_data[$row['id']] = $row;
        //    endwhile;
        //endif;

        # Build the email receipt arrays.


            # The customer.
            $email_array['client']['subject'] = 'Awesome move.';

            $email_array['client']['addresses'] = array($email => $client_name);
	   		//$email_array['client']['addresses'] = array('james@fconlinemarketing.com');

		   $email_array['client']['bcc'] = array('printsupport@fconlinemarketing.com');

            $email_array['client']['body'] = "Hey, this is Mike Parrella.\n\n";

			$email_array['client']['body'] .= "I just wanted to take a moment to let\n"
										   .  "you know you're officially confirmed for\n"
										   .  "my upcoming 1-Day Mastermind.\n\n";

			$email_array['client']['body'] .= "You saw an opportunity, and you took\n"
										   .  "action on it.\n\n";

			$email_array['client']['body'] .= "I love that kind of attitude. Can't wait\n"
										   .  "to meet in person. I know we're going\n"
										   .  "to do big things together.\n\n";

			$email_array['client']['body'] .= "Just to double check, this is the date\n"
										   .  "& location you chose:\n\n";

			$email_array['client']['body'] .= '- ' . $mm_text . "\n\n";

			$email_array['client']['body'] .= "If that is NOT correct, reply to this\n"
										   .  "email and my staff will take care\n"
										   .  "of you.\n\n";

			$email_array['client']['body'] .= "Otherwise...\n\n";

			$email_array['client']['body'] .= "Get ready for the big leagues ;-)\n\n";

			$email_array['client']['body'] .= "It's only up from here.\n\n";

			$email_array['client']['body'] .= "Peace,\nMP\n\n";

            $email_array['client']['body'] .= "-----------------------------------\n\n";

            $email_array['client']['body'] .= "ORDER DETAILS:\n";

            $email_array['client']['body'] .= "ORDER DATE: {$order_date}\n"
                                           .  "ORDER ID: {$invoice_num}\n"
                                           . "Payment Method: {$card_type}\n"
                                          . "CC Num: {$cc_num}\n"
                                          . "CC Exp: {$cc_exp}\n"
                                           .  "Order Total: {$order_total}\n\n\n";

            # The Office Email.
            $email_array['admin']['subject'] = '1-Day MasterMind Receipt';

            $email_array['admin']['addresses'] = array('ryan@fconlinemarketing.com');
			//$email_array['admin']['addresses'] = array('signups@fconlinemarketing.com');
			$email_array['admin']['bcc'] = array('printsupport@fconlinemarketing.com');

            $email_array['admin']['body'] = "ORDER DETAILS:\n\n";

            $email_array['admin']['body'] .= "1 Day MasterMind: {$mm_text}\n\n";

            $email_array['admin']['body'] .= "ORDER DATE: {$order_date} \n"
                                          . "ORDER ID: {$invoice_num}\n";

            if($coupon_code != ''):
                $email_array['admin']['body'] .= "Coupon: {$coupon_code}\n";
            endif;

            $email_array['admin']['body'] .= "Payment Method: {$card_type}\n"
                                          . "CC Num: {$cc_num}\n"
                                          . "CC Exp: {$cc_exp}\n"
                                          . "Order Total: {$order_total} \n\n";

            $email_array['admin']['body'] .= "Name: {$client_name}\n"
                                          . "Email Address: {$email}\n"
                                          . "Phone: {$phone}\n"
                                          . "Address: {$address}\n"
                                          . "City: {$city}\n"
                                          . "State: {$state}\n"
                                          . "Zip: {$zipcode}\n"
                                          . "Country: {$country}";
  /*
echo '<pre>';
print_r($email_array);
echo "<br>";
if(isset($friend_email)):
print_r($friend_email);
endif;
echo "</pre>";
die;
*/
foreach($email_array as $email_to_send):

        $email_failed = $email_to_send;
		//$email_to_send['addresses'] = 'printsupport@fconlinemarketing.com';

		# Loop through the body array, and send each to the respective recipients.
        # Create the message object
        $message = Swift_Message::newInstance()
          # Give the message a subject
          ->setSubject($email_to_send['subject'])
          # Set the From address with an associative array
          ->setFrom(array('info@fconlinemarketing.com' => 'FC Online Marketing'))
          # Set the To addresses with an associative array
          ->setTo($email_to_send['addresses'])
		  # Set BCC
		  ->setBcc($email_to_send['bcc'])
          # Set the reply to address, we want them to all reply to the same address.
          ->setReplyTo(array('info@fconlinemarketing.com' => 'FC Online Marketing'))
          # Add the message body
          ->setBody($email_to_send['body'])
          # And optionally an alternative body
          #->addPart('<q>Here is the message itself</q>', 'text/html')
          # Optionally add any attachments
          #->attach(Swift_Attachment::fromPath('my-document.pdf'))
          ;


        # Send the message
        $emails_result = $mailer->send($message);
        //echo "Emails Result : " . $emails_result . "<br>";
        # Make sure the count is accurate.
        //$smtp_credentials->smtp_relays_used = $smtp_credentials->smtp_relays_used + $emails_result;
        $total_relays = $total_relays + $emails_result;
        //echo "Total Relays : " . $total_relays . "<br>";
        //echo "The Object count : " . $smtp_credentials->smtp_relays_used;
        $process_total = $process_total + $total_relays;
        //echo "<br>" . $emails_result;


endforeach;

$smtp_credentials->smtp_relays_used = $smtp_credentials->smtp_relays_used + $total_relays;

$email_array = array();




                # Update the receipt flag to show these were sent.
                $sql = "UPDATE checkout_orders SET receipt_sent = 1 WHERE order_product_id = '$invoice_num';";

                mysql_query($sql);

                # Write the number of relays used to the database relay count for this SMTP account.

                $qBase = "UPDATE ckt_email_credentials SET smtp_relays_used = %s, smtp_relay_date = '%s' WHERE id = %s;";

                $query = sprintf($qBase,
                    mysql_real_escape_string($smtp_credentials->smtp_relays_used),
                    mysql_real_escape_string(date("Y-m-d")),
                    mysql_real_escape_string($smtp_credentials->id)
                );

                mysql_query($query);


                # Update the total count for the date, if it doesn't exist, insert one.
                $smtp_date = date("Y-m-d");
                $sql = "SELECT * FROM ckt_daily_receipts_sent WHERE date = '$smtp_date';";

                $query_result = mysql_query($sql);

                $row_count = mysql_num_rows($query_result);

                if($row_count > 0):

                    # There is already a row, update it.
                    while($day_count = mysql_fetch_object($query_result)):
                        $current_count = $day_count->receipt_count;
                    endwhile;

                    $total_count = $current_count + $total_relays;

                    # Perform the update.
                    $qBase = "UPDATE ckt_daily_receipts_sent SET receipt_count = %s WHERE date = '%s';";

                    $query = sprintf($qBase,
                        mysql_real_escape_string($total_count),
                        mysql_real_escape_string($smtp_date)
                    );
                else:
                    # We need to add a row.
                    $qBase = "INSERT INTO ckt_daily_receipts_sent (date,receipt_count) VALUES ('%s',%s);";

                    $query = sprintf($qBase,
                        mysql_real_escape_string($smtp_date),
                        mysql_real_escape_string($smtp_credentials->smtp_relays_used)
                    );
                endif;

                mysql_query($query);
    endwhile;

    return $process_total;
}
function email_receipt_mabsdvd()
{
        # This will send the email using the SwiftMailer library.
        # This is a major email revision to the checkout for iLoveKickboxing.com.
        # This method addresses the problem of hitting the email limit on the server,
        # it also resolves the spam index issue that we are experiencing when sending the
        # receipts and registrations.

        global $connection;
        global $table_customers;
        global $table_signups;
        global $price;
        global $email_failed;
        global $sending_account;
        global $developer;

        $process_total = 0;

        # Connect to the database.
        dbConn();

        # Housekeeping procedure: In the event that the date in the database is
        # before today, we want to set the relays used to 0.
        $today = date("Y-m-d");
        $sql = "UPDATE ckt_email_credentials SET smtp_relays_used = 0, smtp_relay_date = '$today' WHERE smtp_relay_date < '$today';";

        # Perform the query
        mysql_query($sql);


        # Query the database for the mail account to use for this email.
        $sql = "SELECT * FROM ckt_email_credentials ORDER BY id DESC;";

        # Get the result set.
        $account_result = mysql_query($sql);

        # Determine the mail account to use for this, we want to ensure that we do not
        # move over the 250 SMTP relays for a given account.
        while($account = mysql_fetch_object($account_result)):
            if($account->smtp_relays_used < 230):
                $smtp_credentials = $account;
            endif;
        endwhile;

        $sending_account = $smtp_credentials->username;

        ###### START: Debug Block ######
        #echo "<pre>";
        #print_r($smtp_credentials);
        #echo "</pre>";
        ###### END: Debug Block ######

        # Create the SMTP transporter.
        $transport = Swift_SmtpTransport::newInstance($smtp_credentials->smtp_server_information, $smtp_credentials->smtp_port)
          ->setUsername($smtp_credentials->username . "@fconlinemarketing.com")
          ->setPassword($smtp_credentials->password)
          ;

        # Create the mailer, use the above created transport.
        $mailer = Swift_Mailer::newInstance($transport);

        # We need to get some information on each of the signups that need receipts.
        $sql = "SELECT
                			co.order_product_id
                FROM
                			checkout_orders co
                WHERE
                			co.order_product_id LIKE 'MABSDVD-%' AND
                			co.receipt_sent = '0'";

        $send_result = mysql_query($sql);

        while($row = mysql_fetch_assoc($send_result)):
        # Set some housekeeping variables.
        $emails_result = 0;
        $email_counter = 0;
        $total_relays = 0;


        //$location = $row['franchise_id'];
        $invoice_num = $row['order_product_id'];


        # Get the school data from the schools table for this school.
        //$school_data = get_location_payment($location);


        #################################################################################
        # START: Exception Handler
        #################################################################################
        # In the even this school does not have an email associated with it.
        # This causes an exception will will cause email not to be sent.
      //  if($school_data['franchise_email'] == ''):
            # Set it to email to signups instead...
       //     $school_data['franchise_email'] = "signups@ilovekickboxing.com";

            # Notify the developer that there is a missing email.
       //     mail("$developer","ERROR: Missing Franchise Email","{$school_data['Name']} with an ID of {$row['franchise_id']}\n Does not have an email address associated wth it.");
       // endif;
        #################################################################################
        # END: Exception Handler
        #################################################################################


        # Get the customer data from the  checkout tables for this customer.
       // $sql = "SELECT cc.*, cs.* FROM ckt_customers cc, ckt_signups cs WHERE cs.customer_id = cc.customer_id AND cc.id = $client_id";
	           $sql = "SELECT
                			cc.customer_id,
							cc.firstname AS first_name,
                			cc.lastname AS last_name,
                			cc.email,
                            cc.phone,
                            cc.password,
							co.subscription_type_id,
                			co.order_total,
                			co.cc_type,
                            co.cc_num,
                			co.cc_exp_month,
                			co.cc_exp_year,
                			co.order_date,
                			co.order_product_id,
                            co.coupon_code
                FROM
                			checkout_orders co,
                			checkout_customers cc
                WHERE
                			cc.customer_id = co.customer_id AND
                			co.order_product_id = '$invoice_num'";


        $customer_result = mysql_query($sql);



        while($row = mysql_fetch_assoc($customer_result)):

	        $customer_id = $row['customer_id'];
            $client_first = ucwords(trim($row['first_name']));
            $client_last = ucwords(trim($row['last_name']));
            $client_name = $client_first . " " . $client_last;
            $phone = $row['phone'];
            $email = strtolower(trim($row['email']));
            $pwd = trim($row['password']);

            $coupon_code = $row['coupon_code'];
            $order_id = $row['order_product_id'];

            $order_total = '$' . number_format($row['order_total'], 2);



            $order_date = $row['order_date'];
            $order_date = date('m/d/y h:i a', strtotime($order_date));

            $cc_num = $row['cc_num'];
            $card_type = $row['cc_type'];
            $cc_exp = $row['cc_exp_month'] . '/' . $row['cc_exp_year'];

        endwhile;


        # Build the email receipt arrays.


            # The customer.
            $email_array['client']['subject'] = 'MABS DVD Receipt';

            $email_array['client']['addresses'] = array($email => $client_name);
	   		//$email_array['client']['addresses'] = array('james@fconlinemarketing.com');

		   $email_array['client']['bcc'] = array('printsupport@fconlinemarketing.com');

            $email_array['client']['body'] = "Thanks for signing up.\n";

            $email_array['client']['body'] = "You will now have instant access to the video tape.\n\n";



            $email_array['client']['body'] .= "Login Details\n";

            $email_array['client']['body'] .= "-----------------------------------\n\n";

            $email_array['client']['body'] .= "Username: {$email}\n";

            $email_array['client']['body'] .= "Password: {$pwd}\n";

            $email_array['client']['body'] .= "URL: https://www.martialartsbusinesssummit.com/mabs_dvd/\n\n";

             $email_array['client']['body'] .= "Customer Details\n";

            $email_array['client']['body'] .= "-----------------------------------\n\n";

            $email_array['client']['body'] .= "Name: {$client_name}\n";

            $email_array['client']['body'] .= "Email: {$email}\n";

            $email_array['client']['body'] .= "Phone: {$phone}\n\n";



            $email_array['client']['body'] .= "Order Details\n";

            $email_array['client']['body'] .= "-----------------------------------\n\n";

            $email_array['client']['body'] .= "Order Date: {$order_date}\n"
                                           .  "Order ID: {$invoice_num}\n"
                                           .  "Payment Method: {$card_type}\n"
                                           .  "CC Num: {$cc_num}\n"
                                           .  "CC Exp: {$cc_exp}\n"
                                           .  "Order Total: {$order_total}\n\n\n";

            # The Office Email.
            $email_array['admin']['subject'] = 'NEW: MABS DVD Order';


            $email_array['admin']['addresses'] = array('james@fconlinemarketing.com');

		   $email_array['admin']['bcc'] = array('printsupport@fconlinemarketing.com');

            $email_array['admin']['body'] = "Hey, thanks for signing up.\n\n";



            $email_array['admin']['body'] .= "Login Details\n";

            $email_array['admin']['body'] .= "-----------------------------------\n\n";

            $email_array['admin']['body'] .= "Username: {$email}\n";

            $email_array['admin']['body'] .= "Password: {$pwd}\n\n";

             $email_array['admin']['body'] .= "Customer Details\n";

            $email_array['admin']['body'] .= "-----------------------------------\n\n";

            $email_array['admin']['body'] .= "Name: {$client_name}\n";

            $email_array['admin']['body'] .= "Email: {$email}\n";

            $email_array['admin']['body'] .= "Phone: {$phone}\n\n";



            $email_array['admin']['body'] .= "Order Details\n";

            $email_array['admin']['body'] .= "-----------------------------------\n\n";

            $email_array['admin']['body'] .= "Order Date: {$order_date}\n"
                                           .  "Order ID: {$invoice_num}\n"
                                           .  "Payment Method: {$card_type}\n"
                                           .  "CC Num: {$cc_num}\n"
                                           .  "CC Exp: {$cc_exp}\n"
                                           .  "Order Total: {$order_total}\n\n\n";



foreach($email_array as $email_to_send):

        $email_failed = $email_to_send;
		//$email_to_send['addresses'] = 'printsupport@fconlinemarketing.com';

		# Loop through the body array, and send each to the respective recipients.
        # Create the message object
        $message = Swift_Message::newInstance()
          # Give the message a subject
          ->setSubject($email_to_send['subject'])
          # Set the From address with an associative array
          ->setFrom(array('info@fconlinemarketing.com' => 'FC Online Marketing'))
          # Set the To addresses with an associative array
          ->setTo($email_to_send['addresses'])
		  # Set BCC
		  ->setBcc($email_to_send['bcc'])
          # Set the reply to address, we want them to all reply to the same address.
          ->setReplyTo(array('info@fconlinemarketing.com' => 'FC Online Marketing'))
          # Add the message body
          ->setBody($email_to_send['body'])
          # And optionally an alternative body
          #->addPart('<q>Here is the message itself</q>', 'text/html')
          # Optionally add any attachments
          #->attach(Swift_Attachment::fromPath('my-document.pdf'))
          ;


        # Send the message
        $emails_result = $mailer->send($message);
        //echo "Emails Result : " . $emails_result . "<br>";
        # Make sure the count is accurate.
        //$smtp_credentials->smtp_relays_used = $smtp_credentials->smtp_relays_used + $emails_result;
        $total_relays = $total_relays + $emails_result;
        //echo "Total Relays : " . $total_relays . "<br>";
        //echo "The Object count : " . $smtp_credentials->smtp_relays_used;
        $process_total = $process_total + $total_relays;
        //echo "<br>" . $emails_result;


endforeach;

$smtp_credentials->smtp_relays_used = $smtp_credentials->smtp_relays_used + $total_relays;

$email_array = array();




                # Update the receipt flag to show these were sent.
                $sql = "UPDATE checkout_orders SET receipt_sent = 1 WHERE order_product_id = '$invoice_num';";

                mysql_query($sql);

                # Write the number of relays used to the database relay count for this SMTP account.

                $qBase = "UPDATE ckt_email_credentials SET smtp_relays_used = %s, smtp_relay_date = '%s' WHERE id = %s;";

                $query = sprintf($qBase,
                    mysql_real_escape_string($smtp_credentials->smtp_relays_used),
                    mysql_real_escape_string(date("Y-m-d")),
                    mysql_real_escape_string($smtp_credentials->id)
                );

                mysql_query($query);


                # Update the total count for the date, if it doesn't exist, insert one.
                $smtp_date = date("Y-m-d");
                $sql = "SELECT * FROM ckt_daily_receipts_sent WHERE date = '$smtp_date';";

                $query_result = mysql_query($sql);

                $row_count = mysql_num_rows($query_result);

                if($row_count > 0):

                    # There is already a row, update it.
                    while($day_count = mysql_fetch_object($query_result)):
                        $current_count = $day_count->receipt_count;
                    endwhile;

                    $total_count = $current_count + $total_relays;

                    # Perform the update.
                    $qBase = "UPDATE ckt_daily_receipts_sent SET receipt_count = %s WHERE date = '%s';";

                    $query = sprintf($qBase,
                        mysql_real_escape_string($total_count),
                        mysql_real_escape_string($smtp_date)
                    );
                else:
                    # We need to add a row.
                    $qBase = "INSERT INTO ckt_daily_receipts_sent (date,receipt_count) VALUES ('%s',%s);";

                    $query = sprintf($qBase,
                        mysql_real_escape_string($smtp_date),
                        mysql_real_escape_string($smtp_credentials->smtp_relays_used)
                    );
                endif;

                mysql_query($query);
    endwhile;

    return $process_total;
}



function getCoupon($id){
    # Get the coupon data for the selected coupon.
    global $table_coupon;

    dbConn();

    $sql = "SELECT * FROM $table_coupon WHERE id=$id;";

    $result = mysql_query($sql);

    $row_count = mysql_num_rows($result);

    if($row_count):
        while($row = mysql_fetch_assoc($result)):
            $coupon = $row;
        endwhile;

        return $coupon;

    endif;

    # If there are no records, then return an empty string.
    return '';

}

function get_location_payment($id){
    # Get the location information for the selected school.

    dbConn();
    //echo $url; die;
    $sql = "SELECT s.*, st.Code FROM Schools s, States st WHERE s.StateID = st.ID AND s.ID =$id;";

    //echo $sql; die;

    $result = mysql_query($sql);

    while($row = mysql_fetch_assoc($result)):
        $data = $row;
    endwhile;

    return $data;

}

function calculate_price($deal, $friends, $coupon = '') {
    # Calculate the cost of the order from the database, and sent elements, this is to
    # firebug safe the checkout.

    $price = $deal['deal_price'];
    $offer_count = $friends + 1;
    $order_total = $offer_count * $price;

    # Test for a coupon.
    if($coupon != ''):
        # There is a coupon, set the discount amount.
        if($coupon['type'] == 'pct'):
            $coupon_amount = $coupon['amount'] / 100;
            $coupon_amount = number_format($coupon_amount,2);
            $discountamount = $coupon_amount;
            $order_total = $order_total - ($order_total * $discountamount);
            $order_total = number_format($order_total,2);
        else:
            $discountamount = $coupon['amount'];
            $order_total = $order_total - $discountamount;
        endif;

        return $order_total;
    endif;

    return $order_total;

}

function handle_exception($e){
    # This method will be our error trap.

    global $email_failed;
    global $sending_account; # We use this in the event the account is marked as overquota.



    dbConn();

    # Here we want to ensure that the emails continue, so we want to determine the error,
    # and make sure it is cleared. A most problematic event is when an address has been
    # marked as overquota so we will look for that here and fix it.


    # Get the message.
    $error_message = $e->getMessage();

    //$error_message = 'Expected response code 250 but got code "550", with message "550 User signups1@ilovekickboxing.com has exceeded their send quota"\'"';
    //$error_message = "This is a test";

    if(strchr($error_message,"550 User")):
        # This account is listed as over quota, mark it as unusable for the rest of the day.
        $sql = "UPDATE ckt_email_credentials SET smtp_relays_used = 230 WHERE username = '$sending_account';";

        # Perform the query.
        mysql_query($sql);
    endif;

return $e;

}