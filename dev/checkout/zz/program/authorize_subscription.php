<?php


		

		//build xml to post
		$content = $subscription_interval;
		        "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		        	"<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .

					"<merchantAuthentication>".
		        		"<name>" . AUTHORIZENET_API_LOGIN_ID . "</name>".
		        		"<transactionKey>" . AUTHORIZENET_TRANSACTION_KEY . "</transactionKey>".
		        	"</merchantAuthentication>".

				   //	"<refId>" . $refId . "</refId>".

					"<subscription>".
		        		"<name>" . $subscription_name . "</name>".

						"<paymentSchedule>".
		        			"<interval>".
		        				"<length>". $subscription_length ."</length>".
		        				"<unit>". $subscription_interval ."</unit>".
		        			"</interval>".

							"<startDate>" . $startDate . "</startDate>".
		        			"<totalOccurrences>". $totalOccurrences . "</totalOccurrences>".
		        		"</paymentSchedule>".

						"<amount>". $subscription_amount ."</amount>".


						"<payment>".
		        			"<creditCard>".
		        				"<cardNumber>" . $cc_num . "</cardNumber>".
		        				"<expirationDate>" . $cc_exp_year . '-' . $cc_exp_month . "</expirationDate>".
								"<cardCode>" . $cc_code . "</cardCode>".
		        			"</creditCard>".
		        		"</payment>".

						"<order>" .
							"<description>" . $subscription_description . "</description>" .
						"</order>" .
						

						"<billTo>".
		        			"<firstName>". $firstname . "</firstName>".
		        			"<lastName>" . $lastname . "</lastName>".
							"<address>" . $fulladdress . "</address>".
							"<city>" . $city . "</city>".
							"<state>" . $state . "</state>".
							"<zip>" . $zipcode . "</zip>".
							"<country>" . $country . "</country>".
		        		"</billTo>".

						"<shipTo>".
		        			"<firstName>". $firstname . "</firstName>".
		        			"<lastName>" . $lastname . "</lastName>".
							"<address>" . $fulladdress . "</address>".
							"<city>" . $city . "</city>".
							"<state>" . $state . "</state>".
							"<zip>" . $zipcode . "</zip>".
							"<country>" . $country . "</country>".
		        		"</shipTo>".

		        	"</subscription>".
		        "</ARBCreateSubscriptionRequest>";

		      
		//send the xml via curl
		$response = send_request_via_curl($host,$path,$content);
	   
		//if the connection and send worked $response holds the return from Authorize.net
		if ($response)
		{
			/*
			a number of xml functions exist to parse xml results, but they may or may not be avilable on your system
			please explore using SimpleXML in php 5 or xml parsing functions using the expat library
			in php 4
			parse_return is a function that shows how you can parse though the xml return if these other options are not avilable to you
			*/
			list ($refId, $resultCode, $code, $text, $subscriptionId) = parse_return($response);
			$subscription_id = $subscriptionId;
			$tmp = $code;
			$subscription_response = $text;


		}
		else
		{
			echo "Transaction Failed. <br>";
		}





?>