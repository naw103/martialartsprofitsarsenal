/*
 *
 * Perform ALL Cufon replacements in this file
 *
 */

 Cufon.replace('.futura-med', { fontFamily: 'Futura Medium' });
 Cufon.replace('.futura-heavy', { fontFamily: 'Futura Std Heavy' });
 Cufon.replace('.futura-condensed', { fontFamily: 'Futura-CondensedMedium' });
 Cufon.replace('.futura-oblique', { fontFamily: 'Futura Std Oblique' });
Cufon.replace('nav a', { fontFamily: 'Futura-CondensedMedium' });

 Cufon.replace('#speaker_container .speaker_name', { fontFamily: 'Futura Std Bold', color: '-linear-gradient(#FFF,#b8b8b8)',
 	textShadow: '3px 3px rgba(0,0,0,.2), -3px 3px rgba(0,0,0,.2), 3px -3px rgba(0,0,0,.2), -3px -3px rgba(0,0,0,.2)' });

 Cufon.replace('.where_when div', { fontFamily: 'Futura Medium', color: '-linear-gradient(#ff9307,#d87a00)' });
  Cufon.replace('.where_when div span', { fontFamily: 'Futura Medium', color: '-linear-gradient(#1f364a,#1f364a)' });