<?php
require('../program/program.php');

$order_date = date('Y-m-d H:i:s');
$today = date('Y-m-d H:i:s');

// Check for expired credit card before monthly payments end
$valid_card = FALSE;
$approved = FALSE;
$authorization_code = '';
$transaction_id = '';
$response_reason = '';
$invoice_num = '';
$subscription_id = '';
$paid_ticket_cost = 0;
$actual_ticket_cost = 0;
$coupon_amt = 0;

if(isset($_POST))
{
	# Get all data
	$jt_tmp = '';
	foreach($_POST as $key => $value):
		$$key = trim($value);
		$jt_tmp .= $key . ' => ' . $value . "\n";
	endforeach;
	//mail('james@fconlinemarketing.com', 'mapa', $jt_tmp);
	/*
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	die;*/


	# Load database class
	require('../program/program.php');
    require('../program/definitions.php');
	require('../program/class.checkout.php');
	require('../program/functions.php');

	if($pay_in_full == 2):
        $pay_in_full = 0;
		$subscription_type_id = 2;
    else:
        $pay_in_full = 1;
		$subscription_type_id = 1;
    endif;

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();




		# Get prices from database

        $start_total = 0;
		$ticket_cost = 0;
        $order_total = 0;

        if($ticket == '1'):
            $ticket_info = $db->get_product_info($mapa_id, $pay_in_full, $ez);
            if($pay_in_full):
                $ticket_cost += $ticket_info['cost'];
            else:
                $ticket_cost += $ticket_info['ez_pay']['cost'];
            endif;
            $actual_ticket_cost = $ticket_cost;
        endif;

		$prod_name = $ticket_info['product_name'];
		$prod_abbr = $ticket_info['abbr_name'];
		$prod_code = $ticket_info['product_code'];
		$ezpay_id = $ticket_info['ez_pay']['id'];
		$ezpay_num = $ticket_info['ez_pay']['num'];
		$ezpay_interval = $ticket_info['ez_pay']['interval'];
		$ezpay_cost = $ticket_info['ez_pay']['cost'];





	    // calculate order total based on prices from database

		$coupon_amt = '';
        $subscription_amount = 0;
		$sub_amt = 0;


	    # Check coupon
	    $sql = "SELECT
							*
				  FROM
				  			checkout_coupons cc
				  WHERE
				  			DATE(start_date) <= DATE('$today') AND
							DATE(end_date) >= DATE('$today') AND
							coupon_code = '$coupon_code' AND
							site_id = '$site_id' AND
							active = 1";


		$result = $db->Execute($sql);


	    $coupon = array();
		if($db->getNumRows() > 0):

	        #active coupon
	        $coupon['code'] = $result['coupon_code'];
	        $coupon['amount'] = $result['amount'];
	        $coupon['type'] = $result['type'];
            $coupon['products'] = $result['products'];


			$coupon_result = $db->apply_coupon($coupon, $ticket_cost, $pay_in_full, $ezpay_num);

			$paid_ticket_cost = $coupon_result['ticket_cost'];
			$coupon_amt = $coupon_result['coupon_amt'];

			if($coupon['products'] == '5'):
				$sub_amt = $ticket_cost;
			else:
				$sub_amt = $paid_ticket_cost;
			endif;

		else:

			$paid_ticket_cost = $ticket_cost;
			$sub_amt = $ticket_cost;

	    endif;

		$subscription_amount = $sub_amt;
		$order_total = $paid_ticket_cost;

		if($dollardown):
			$subscription_amount = $sub_amt;
			$order_total = 1;
		endif;

		#echo $order_total . '<br />' . $subscription_amount; die;



        if($order_total > 0):
            $bypass_payment = FALSE;
        else:
            $bypass_payment = TRUE;

			$cc_num = '';
			$cc_name = '';
			$cc_type = '';
			$cc_exp_month = '';
			$cc_exp_year = '';
        endif;

		
		# get state name
		$state = $db->get_state($statecode);

		# get country name
		$country = $db->get_country($countrycode);;

		/*
		 *  Use AuthorizeNet API to post payment
		 *  if approved
		 		- insert record into database
		 		- email customer   	 *
		*/

        if(!$bypass_payment):
    	  	if(date('Y-m', strtotime($order_date)) <= date('Y-m', strtotime($cc_exp_year . '-' . $cc_exp_month))):
    	  	    $valid_card = TRUE;
				/*
                if(!$pay_in_full):

                  $valid_card = FALSE;
                  $t = $ticket_info['ez_num'];
                  if(date('Y-m', strtotime($order_date  . "+ $t month")) <= date('Y-m', strtotime($cc_exp_year . '-' . $cc_exp_month))):
        				$valid_card = TRUE;

        		  else:
        		        //$response_reason = 'Credit Card Expires before ' . $t .'th Monthly Payment';
						$valid_card = TRUE;

        		  endif;


                endif;
				 */
    	  	else:
    	  		$response_reason = 'Credit Card is Expired';
    	  	endif;
        endif;

        $db->all_orders($site_id);
        $all_orders = $db->getNumRows();


       /* if($all_orders >= $max_ppl):
            $valid_card = FALSE;
            $response_reason = 'Sorry. Registration is now closed. You have not been charged.';
        else:
            $valid_card = TRUE;
        endif;
       */



	  	if($valid_card)
	  	{

	        # connect to authorize.net
	  	    require('../anet_php_sdk/AuthorizeNet.php'); // The SDK

	        $sale = new AuthorizeNetAIM;
	        $sale->setSandbox(AUTHORIZENET_SANDBOX);
	        $sale->test_request = TEST_REQUEST;

	        $sale->setFields(
	                array(
	        	            'amount' => $order_total,
	                        'card_num' => $cc_num,
	                        'card_code' => $cc_code,
	                        'exp_date' => $cc_exp_month . "/" . $cc_exp_year,
	                        'first_name' => $firstname,
	                        'last_name' => $lastname,
	                        'address' => $address . " " . $address2,
	                        'city' => $city,
	                        'state' => $state,
	                        'country' => $country,
	                        'zip' => $zipcode,
	                        'phone' => $phone,
	                        'email' => $email,
	        				'invoice_num' => $prod_abbr,
	        				'description' => $prod_name,
							'duplicate_window' => 10
	        	)
	        );


	        // Authorize payment
	        $response = $sale->authorizeAndCapture();




	        $approved = $response->approved;
			$authorization_code = $response->authorization_code;
		    $transaction_id = $response->transaction_id;
	        #$response_reason = $response->response_reason_text;

			$response_reason = $response->response_reason_text;
	        $response_reason_code = $response->response_reason_code;

	        if($response_reason_code == 65):
	            $response_reason = 'Transaction Decline: Card Code Mismatch';
	        endif;

	  		// check payment response
	  		if ($approved)
	  		{

                # Payment Successful, set up subscription in neccessary
                if(!$pay_in_full):

                    $subscription_name = $prod_code;
                    $subscription_length = 1;

					if($ezpay_num == '9999'):
                    	$totalOccurrences = $ezpay_num;
					else:
						$totalOccurrences = $ezpay_num - 1;
					endif;

                    $subscription_interval = $ezpay_interval;
                    $time = strtotime($order_date);
			        $startDate = date("Y-m-d", strtotime("+1 month", $time));
                    //$subscription_amount = $ticket_info['ez_cost'];
                    $subscription_description = $prod_name;


                    //build xml to post
            		$content =
            		        "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
            		        	"<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .

            					"<merchantAuthentication>".
            		        		"<name>" . AUTHORIZENET_API_LOGIN_ID . "</name>".
            		        		"<transactionKey>" . AUTHORIZENET_TRANSACTION_KEY . "</transactionKey>".
            		        	"</merchantAuthentication>".

            				   //	"<refId>" . $refId . "</refId>".

            					"<subscription>".
            		        		"<name>" . $subscription_name . "</name>".


            						"<paymentSchedule>".
            		        			"<interval>".
            		        				"<length>". $subscription_length ."</length>".
            		        				"<unit>". $subscription_interval ."</unit>".
            		        			"</interval>".

            							"<startDate>" . $startDate . "</startDate>".
            		        			"<totalOccurrences>". $totalOccurrences . "</totalOccurrences>".
            		        		"</paymentSchedule>".

            						"<amount>". $subscription_amount ."</amount>".


            						"<payment>".
            		        			"<creditCard>".
            		        				"<cardNumber>" . $cc_num . "</cardNumber>".
            		        				"<expirationDate>" . $cc_exp_year . '-' . $cc_exp_month . "</expirationDate>".
            								"<cardCode>" . $cc_code . "</cardCode>".
            		        			"</creditCard>".
            		        		"</payment>".

            						"<order>" .
            							"<description>" . $subscription_description . "</description>" .
            						"</order>" .


            						"<billTo>".
            		        			"<firstName>". $firstname . "</firstName>".
            		        			"<lastName>" . $lastname . "</lastName>".
            							"<address>" . $fulladdress . "</address>".
            							"<city>" . $city . "</city>".
            							"<state>" . $state . "</state>".
            							"<zip>" . $zipcode . "</zip>".
            							"<country>" . $country . "</country>".
            		        		"</billTo>".

            						"<shipTo>".
            		        			"<firstName>". $firstname . "</firstName>".
            		        			"<lastName>" . $lastname . "</lastName>".
            							"<address>" . $fulladdress . "</address>".
            							"<city>" . $city . "</city>".
            							"<state>" . $state . "</state>".
            							"<zip>" . $zipcode . "</zip>".
            							"<country>" . $country . "</country>".
            		        		"</shipTo>".

            		        	"</subscription>".
            		        "</ARBCreateSubscriptionRequest>";


            		//send the xml via curl
            		$response = send_request_via_curl($host,$path,$content);
					
            		//if the connection and send worked $response holds the return from Authorize.net
            		if ($response)
            		{
            			/*
            			a number of xml functions exist to parse xml results, but they may or may not be avilable on your system
            			please explore using SimpleXML in php 5 or xml parsing functions using the expat library
            			in php 4
            			parse_return is a function that shows how you can parse though the xml return if these other options are not avilable to you
            			*/
            			list ($refId, $resultCode, $code, $text, $subscriptionId) = parse_return($response);
            			$subscription_id = $subscriptionId;
            			$tmp = $code;
            			$subscription_response = $text;
                        

            		}
            		else
            		{
            			echo "Transaction Failed. <br>";
            		}

                endif;


                include('../program/database_queries.php');


	            # Send Email
	            // This is where we would send our receipt email.
	            // We are gonna handle this using a chron job.


	  		}

	  	}
        elseif($bypass_payment)
        {
            $approved = TRUE;
            $valid_card = TRUE;

            include('../program/database_queries.php');
        }
        // end approved


$db->close();

if($approved == 0):

	$to = 'printsupport@fconlinemarketing.com';
	$subject = 'Checkout Error Validator';
	$headers = 'From: "FC Online Marketing" <info@fconlinemarketing.com>' . "\r\n" .
    		   'Reply-To: info@fconlinemarketing.com' . "\r\n" .
        	   'X-Mailer: PHP/' . phpversion();

	$message = "Approved: Failed\n";
	$message .= "Invoice: {$invoice_num}\n";
	$message .= "Valid Card: {$valid_card}\n";
	$message .= "Response: {$response_reason}\n\n";

	$message .= "Name: {$firstname} {$lastname}\n";
	$message .= "Email: {$email}\n";
	$message .= "Phone: {$phone}\n";

   //	mail($to, $subject, $message, $headers);

endif;

header('Content-type: text/xml');
header('Cache-control: no-cache');
echo "<?xml version=\"1.0\" ?>\n";
echo "<response>\n";

echo "\t<result>\n";

echo "\t\t<approved>" . $approved . "</approved>\n";
echo "\t\t<invoice_num>" . $invoice_num . "</invoice_num>\n";
echo "\t\t<valid_card>" . $valid_card . "</valid_card>\n";
echo "\t\t<authorization_code>" . $authorization_code . "</authorization_code>\n";
echo "\t\t<transaction_id>" . $transaction_id . "</transaction_id>\n";
echo "\t\t<response_reason>" . $response_reason . "</response_reason>\n";

echo "\t</result>\n";

echo "</response>";



}

?>