<?php
    if($_GET['id'] == ''):
        header('Location: index.php');
    endif;

	require('program/program.php');
	require('program/definitions.php');
	require('program/class.checkout.php');
	require('program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

    $id = $_GET['id'];

    $order = $db->order_details($id);

	$ez_info = $db->get_ez_options($ticket_id, $order['ez_pay']);

	$t = '$' . $order['paid_ticket_cost'];


	$monthly_payment = $order['order_total'];
	if($order['coupon_code'] == 'blackfriday' || $order['coupon_code'] == '1-dollar'):
		$monthly_payment = '$97';
	endif;



	if((int)$order['subscription_type_id'] == 2):
		if(number_format($order['order_total']) != 0):
				$order_total = number_format($order['order_total'], 2);

				$order_total = '$' . number_format(((str_replace('$', '', $order_total))), 2) . " (Ongoing Monthly Payments of {$monthly_payment})";
		else:
			$order_total = '$' . number_format($order['order_total'], 2);
		endif;
	else:
    	$order_total = '$' . number_format($order['order_total'], 2);
	endif;

	$db->close();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Profits Arsenal</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="../css/landing-page.css" rel="stylesheet">
	<link rel="stylesheet" href="css/styles.css"/>
<link rel="stylesheet" href="css/signup.css" />

<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>





<style>

#page_content p {
  font-size: 15px;
  margin: auto 135px 15px; }

table {
  width: 680px;
  font-size: 15px;
  margin: 45px 135px 0px;
}



    table thead {
      font-size: 18px;
      background: #DDDDDD; }

	  table thead td { padding: 7px 5px; }

</style>

<!-- Facebook Conversion Code for conversion pic for FCOM websites -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6007882711258', {'value':'67.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6007882711258&amp;cd[value]=67.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
</head>

<body>

    <div class="intro-header" style="background: none;">

        <div class="container">
	<!-- BEGIN: Page Content -->
	<div id="page_content">

		<div id="headline_container"></div>

		    <h1 class="futura" style="text-align: center; font-size: 40px; margin-bottom: 30px;">Thank you for your purchase.</h1>

            <p>
                Hey <?php echo stripslashes($order['first_name']); ?>,
            </p>

            <p>
                We are excited for you to be joining us with MAPA. You will receive
                an email with receipt of your purchase to <?php echo stripslashes($order['email']); ?>.
            </p>

            <p>
                Ryan or myself will reach out to your shortly with any other details that you
                will need to know.
            </p>

            <p>
                If you have any other questions, don't hesistate to reach out to us yourself.
                Call (516) 543-0041.
            </p>

            <table>
                <thead>
					<tr>
						<td>Customer Details</td>
						<td>Order Details</td>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>
			                Name: <?php echo stripslashes($order['first_name']); ?> <?php echo stripslashes($order['last_name']); ?><br />
			                Address: <?php echo stripslashes($order['address']); ?><br />
			                City: <?php echo stripslashes($order['city']); ?><br />
			                State: <?php echo stripslashes($order['Code']); ?><br />
			                Zip: <?php echo stripslashes($order['zipcode']); ?><br />
			                Country: <?php echo stripslashes($order['CountryCode']); ?><br />
							Phone: <?php echo stripslashes($order['phone']); ?><br />
							Email: <?php echo stripslashes($order['email']); ?>
						</td>

						<td>
			                Invoice #: <?php echo stripslashes($order['order_product_id']); ?><br />
							<?php if(number_format($order['order_total']) != 0): ?>
			                CC: <?php echo stripslashes($order['cc_num']); ?><br />
			                CC Exp: <?php echo stripslashes($order['cc_exp_month']); ?>/<?php echo stripslashes($order['cc_exp_year']); ?><br />
			                CC Type: <?php echo stripslashes($order['cc_type']); ?><br />
							<?php endif; ?>
							<?php if($order['coupon_code'] != ''): ?>Coupon: <?php echo stripslashes($order['coupon_code']); ?><br /><?php endif; ?>
			                Total: <?php echo $order_total; ?>
						</td>
					</tr>

				</tbody>
            </table>



	</div>
	<!-- END: Page Content -->
        </div>
    </div>
    <!-- /.container -->



    <footer style='text-align: center; padding-top: 50px;'>
      <div class="container">
			<section class="row breath">
				<div class="col-md-12 footerlinks">
					<p>&copy; 2014 Parrella Consulting Inc. All Rights Reserved</p>
				</div>
			</section><!--/section -->
		</div>
    </footer>


<div id="alert_overlay"></div>
<div id="alert_container">
    <div id="alert_copy">

    </div>
    <input type="button" id="alert_ok" value="OK" />
</div>

    <script src="../js/bootstrap.js"></script>

</body>

</html>
