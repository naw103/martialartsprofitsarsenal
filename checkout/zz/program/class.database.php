<?php

# include database definitions so we do not need to include them everytime
require('definitions.php');


# This class will handle the interactions between the program and the database

class database
{

	var $host = NULL;
	var $username = NULL;
	var $password = NULL;
	var $dbName = NULL;
	var $queryResult = NULL;
	var $connection = NULL;

	function __construct($host, $username, $password, $database)
	{
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->dbName = $database;

	}


	function open()
	{
		$this->connection = mysql_connect($this->host, $this->username, $this->password);

		if($this->connection)
		{
			$mysql_db = mysql_select_db($this->dbName);

			if(!$mysql_db):
				echo "Can't Use database: " . mysql_error();
			endif;

		}
		else
		{
			echo "Can't Connect to database: " . mysql_error();
		}

	}


	function close()
	{
		mysql_close($this->connection);
	}


	function Execute($sql)
	{
		if(strtoupper(substr($sql,0,6)) == 'SELECT'):
			$this->queryResult = mysql_query($sql);

			return $this->fetchAssoc();
		else:
			//echo $sql; 
			return mysql_query($sql);

		endif;

	}



	function getNumRows()
	{
		return mysql_num_rows($this->queryResult);
	}

	function fetchAssoc()
	{

		while($row = mysql_fetch_assoc($this->queryResult)):

			($this->getNumRows() == 1) ? $data = $row : $data[] = $row;

		endwhile;

		return $data;

	}

    function ezpay_options($product_id)
    {
        # This method will return all ez pay options
        # for the particular product
        $data = array();

        $sql = "SELECT
                        ce.*
                FROM
                        checkout_product_ezpay_join cpo,
                        checkout_ezpay ce
                WHERE
                        ce.id = cpo.ezpay_id AND
                        cpo.active = '1' AND
                        cpo.product_id = '$product_id'";


        $data = $this->Execute($sql);


        return $data;
    }


}


?>