<?php

require('../anet_php_sdk/AuthorizeNet.php'); // The SDK

if(isset($invoice_code) && $invoice_code != ''):
	$invoice_code = 'MABS14MASTERMIND';
else:
	$invoice_code = 'MABS14';
endif;

$sale = new AuthorizeNetAIM;
$sale->setSandbox(AUTHORIZENET_SANDBOX);
$sale->test_request = TEST_REQUEST;

$sale->setFields(
        array(
	        'amount' => $order_total,
                'card_num' => $cc_num,
                'card_code' => $cc_code,
                'exp_date' => $cc_exp_month . "/" . $cc_exp_year,
                'first_name' => $firstname,
                'last_name' => $lastname,
                'address' => $address . " " . $address2,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'zip' => $zipcode,
                'phone' => $phone,
                'email' => $email,
				'invoice_num' => $invoice_code,
				'description' => 'Martial Arts Business Summit 2014'
	)
);

// Aurthorize payment
$response = $sale->authorizeAndCapture();


?>