$(document).ready(function() {


	// If State DropDown is changed
	$('select[name="country"]').change(function()
	{
		var country_id = $(this).val();


		$.post(
				"ajax/get_states.php",
				{ country_id: country_id },
				function(xml)
				{
					var option = '<option value="">Please Select</option>';

					$(xml).find('response state').each(function()
					{
						var state_id = $(this).find('state_id').text();
						var state = $(this).find('state_name').text();

						option += '<option value="' + state_id + '">' + state + '</option>';

					});

					$('select[name="state"]').html(option);


				}
		);

	});

	$('input#checkout_now').click(function(){

		if($('#loader').is(':hidden'))
		{

			validateForm($('form[name="checkout"]'));

			if($('form[name="checkout"]').find('input, select').hasClass('error'))
			{
				alert("Must Fill Out All Required Information");

			}
			else
			{

				var uid = $('#form_wrapper').attr('data-uid');


				// Credit Card Info
				var cc_name = $('input[name="cc_name"]').val();
				var cc_num = $('input[name="cc_num"]').val();
				var cc_type = '';

				$('ul.cards').find('li').each(function()
				{
					if(!$(this).hasClass('off'))
					{
						cc_type = $(this).attr('class');
					}

				});

				var cc_exp_month = $('select[name="cc_exp_month"]').val();
				var cc_exp_year = $('select[name="cc_exp_year"]').val();
				var cc_code = $('input[name="cvv2"]').val();

				$('div#loader').fadeIn('fast');


				if($('input[name="firstname"]').length > 0)
				{
					// Contact Info
					var firstname = $('input[name="firstname"]').val();
					var lastname = $('input[name="lastname"]').val();
					var address = $('input[name="address"]').val();
					var address2 = $('input[name="address2"]').val();
					var city = $('input[name="city"]').val();
					var zipcode = $('input[name="zipcode"]').val();
					var statecode = $('select[name="state"]').val();
					var countrycode = $('select[name="country"]').val();
					var phone = $('input[name="phone"]').val();
					var email = $('input[name="email"]').val();

					$.ajax({
							url: 'ajax/mastermind1.php',
							type: 'POST',
						   //	dataType: 'xml',
							data: {
								uid: uid,

								firstname: firstname,
								lastname: lastname,
								address: address,
								address2: address2,
								city: city,
								zipcode: zipcode,
								statecode: statecode,
								countrycode: countrycode,
								phone: phone,
								email: email,

								cc_name: cc_name,
								cc_num: cc_num,
								cc_type: cc_type,
								cc_exp_month: cc_exp_month,
								cc_exp_year: cc_exp_year,
								cc_code: cc_code
							},
							complete: function(data, status)
							{
								//console.log(data);
								notify(data.responseText);
							}
					});


				}
				else
				{

					$.ajax({
							url: 'ajax/mastermind.php',
							type: 'POST',
						   //	dataType: 'xml',
							data: {
								uid: uid,

								cc_name: cc_name,
								cc_num: cc_num,
								cc_type: cc_type,
								cc_exp_month: cc_exp_month,
								cc_exp_year: cc_exp_year,
								cc_code: cc_code
							},
							complete: function(data, status)
							{
								//console.log(data);
								notify(data.responseText);
							}
					});

				}

			}

		}

	});


	function notify(data){

		var xml = data,
		xmlDoc = jQuery.parseXML( xml ),
		$xml = $( xmlDoc ),
		$status = $xml.find("status").text(),
		$response = $xml.find("response_text").text();

		if(parseInt($status) == 1)
		{

			$('div#loader').fadeOut('fast');
			window.location.href = 'thank-you-mastermind.php';
		}
		else
		{
			$('div#loader').fadeOut('fast');
			alert($response);
		}

	}


	// FORM VALIDATION
	function validateForm(form)
	{
	   form.find('input.required, select.required').each(function()
	   {
			if($(this).val() == '')
			{
				$(this).addClass('error');
			    $(this).removeClass('valid');

				if(($(this).attr('title') != '') && $(this).next('div.error_msg').length == 0)
				{
					if($(this).attr('title'))
					{
						$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
					}
				}

			}
			else
			{
			   if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $('input[name="email"]').val();

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');

					if($(this).next('div.error_msg').length == 0)
					{
						$('<div class="error_msg">Invalid Email</div>').insertAfter($(this));
					}
					else
					{
						$(this).next('div.error_msg').html('<div class="error_msg">Invalid Email</div>');
					}
				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
			}
	   });

	}


	$('input.required').keyup(function()
	{
		if($(this).val() != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $('input[name="email"]').val();

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');

					if($(this).next('div.error_msg').length == 0)
					{
						$('<div class="error_msg">Invalid Email</div>').insertAfter($(this));
					}
					else
					{
						$(this).next('div.error_msg').html('<div class="error_msg">Invalid Email</div>');
					}
				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');

			if($(this).attr('title') != '' && $(this).next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
				}


		}

	});

	$('input.required').blur(function()
	{
		if($(this).val() != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $('input[name="email"]').val();

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');

					if($(this).next('div.error_msg').length == 0)
					{
						$('<div class="error_msg">Invalid Email</div>').insertAfter($(this));
					}
					else
					{
						$(this).next('div.error_msg').html('<div class="error_msg">Invalid Email</div>');
					}
				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');

			if($(this).attr('title') != '' && $(this).next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
				}


		}

	});

	$('input[name="address2"]').keyup(function()
	{
		if($(this).val() != '')
		{
			$(this).addClass('valid');
		}
		else
		{
			$(this).removeClass('valid');
		}

	});




	$('select.required').change(function()
	{
		if($(this).val() != '')
		{
			$(this).removeClass('error');
			$(this).addClass('valid');

			$(this).next('div.error_msg').remove();
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');

			if($(this).attr('title') != '' && $(this).next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
				}
		}

	});


	$('span#cvv2_define').click(function() {



		var left_pos = ($(window).width() / 2) - ($('#cvv2').width() / 2);

			var top_pos = ($(window).height() / 2) - ($('#cvv2').height() / 2)

			$('#cvv2').css({
											'left': left_pos + 'px',
											'top': top_pos + 'px',
											'position': 'fixed'
										});

		$('#cvv2').show();


		$('div#cvv2_overlay').height($(document).height());
		$('div#cvv2_overlay').show();

	});




});