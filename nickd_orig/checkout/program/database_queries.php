<?php

# Create Customer Record
	          	$sql = "INSERT INTO checkout_customers (
												  firstname, lastname, address, address2, city,
												  zipcode, state_id, country_id, phone, email
				 ) VALUES (
												  '%s', '%s', '%s', '%s', '%s',
												  '%s', '%s', '%s', '%s', '%s'
				 )";

	        	$sql = sprintf($sql,
	        							mysql_real_escape_string($firstname),
	        							mysql_real_escape_string($lastname),
	        							mysql_real_escape_string($address),
	        							mysql_real_escape_string($address2),
	        							mysql_real_escape_string($city),

	        							mysql_real_escape_string($zipcode),
	        						    mysql_real_escape_string($statecode),
	        							mysql_real_escape_string($countrycode),
	        							mysql_real_escape_string($phone),
	        							mysql_real_escape_string($email)
	        					);


	        	$db->Execute($sql);

	        	// Get customer_id of newly inserted row
	        	$customer_id = mysql_insert_id();

	            # Create Order Record


	            // mask cc number
	            if($cc_num != ''):
				    $cc_num = substr($cc_num, 0, 4) . str_repeat("X", strlen($cc_num) - 8) . substr($cc_num, -4);
	            endif;



				// get customers ip address
				$ip_address = $_SERVER['REMOTE_ADDR'];

				$sql = "INSERT INTO checkout_orders (
														customer_id, subscription_type_id, ez_pay, ticket_cost, paid_ticket_cost,
														coupon_code, coupon_amt, cc_name, cc_num, cc_type,
														cc_exp_month, cc_exp_year, transaction_id, order_total, order_date,
														tshirt_size, ip_address, subscription_id, hear_about_us, hear_about_other,
														hear_about_friend, receipt_sent
						  ) VALUES (
						  								'%s', '%s', '%s', '%s', '%s',
														'%s', '%s', '%s', '%s', '%s',
                                                        '%s', '%s', '%s', '%s', '%s',
														'%s','%s', '%s',  '%s', '%s',
														'%s', '%s'
						  )";

				$sql = sprintf($sql,
	                                    mysql_real_escape_string($customer_id),
	                                    mysql_real_escape_string($subscription_type_id),
										mysql_real_escape_string($ez),
										mysql_real_escape_string($actual_ticket_cost),
										mysql_real_escape_string($paid_ticket_cost),


                                        mysql_real_escape_string($coupon['code']),

                                        mysql_real_escape_string($coupon_amt),
										mysql_real_escape_string($cc_name),
										mysql_real_escape_string($cc_num),
										mysql_real_escape_string($cc_type),

                                        mysql_real_escape_string($cc_exp_month),
										mysql_real_escape_string($cc_exp_year),
	                                    mysql_real_escape_string($transaction_id),
	                                    mysql_real_escape_string($order_total),
	                                    mysql_real_escape_string($order_date),

                                        mysql_real_escape_string($tshirt_size),
										mysql_real_escape_string($ip_address),
										mysql_real_escape_string($subscription_id),
                                        mysql_real_escape_string($hear_about),
                                        mysql_real_escape_string(trim($hear_about_other)),

                                        mysql_real_escape_string(ucwords(trim($hear_about_friend))),
										mysql_real_escape_string('0')
								);

				$db->Execute($sql);

				// Get order id of newly inserted row
				$order_id = mysql_insert_id();


				// Create unique order id #
				$invoice_num = $ticket_info['product_code'] . '-' . $order_id;

				$sql = "UPDATE
                                    checkout_orders
				        SET
                                    order_product_id = '$invoice_num'
						WHERE
                                    order_id = $order_id";

				$db->Execute($sql);


				# PRODUCTS JOIN TABLE

                if($ticket):

                    // add ticket
    				$sql = "INSERT INTO checkout_products_join (
    																product_id, order_id, paid, actual_cost
    						  ) VALUES (
    						  										'%s', '%s', '%s', '%s'
    						  )";

                    $sql = sprintf($sql,
                                        mysql_real_escape_string($ticket_info['product_id']),
                                        mysql_real_escape_string($order_id),
										mysql_real_escape_string($paid_ticket_cost),
										mysql_real_escape_string($ticket_info['cost'])
                                  );

    				$db->Execute($sql);

                endif;


?>