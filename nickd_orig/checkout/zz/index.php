<?php

	include('program/program.php');
	include('program/class.database.php');

	$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	$db->open();

    $ez_pay = array();
    $ez_pay = $db->ezpay_options($ticket_id);
    

	$countries = $db->Execute("SELECT * FROM Countries");

	$states = $db->Execute("SELECT * FROM States WHERE Country = 1");

	$db->close();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Martial Arts Business Summit | March 28-30, 2014 in New York City</title>

	<meta name="description" content="">

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<link rel="stylesheet" href="css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="css/signup.css?ver=1.0" />
	<link rel="stylesheet" href="css/countdown.css?ver=1.0" />

	<script src="https://www.ilovekickboxing.com/intl_js/jquery.js?ver=1.0"></script>

	<!--[if lt IE 9]>
  		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script>
  		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  		ga('create', 'UA-29420841-55', 'martialartsbusinesssummit.com');
  		ga('send', 'pageview');

	</script>

</head>
<body>

<!-- BEGIN: Page Content -->
<div id="container">
	<div id="page_content">

		<?php include('header.php'); ?>

		<div id="the_guarantee">

			<img src="images/guaranteed.png" alt="" />

			<p>
				If you decide during MABS that the info is not cutting edge and will not help you with your business then let our support staff know immediately and we'll give you a 200% refund on the spot.
			</p>

		</div>

		<div id="form_wrapper" data-payment="<?php echo $_GET['ez']; ?>">

			<form name="checkout" method="POST" action="">

				<!-- CONTACT INFO -->
				<div class="section futura-heavy">Contact Info</div>

				<label for="firstname">First Name:</label>
				<input type="text" name="firstname" placeholder="First Name" title="Enter Your First Name" class="large required" /><br />

				<label for="lastname">Last Name:</label>
				<input type="text" name="lastname" placeholder="Last Name" title="Enter Your Last Name" class="large required" /><br />

				<label for="address">Address:</label>
				<input type="text" name="address" placeholder=" Address" title="Enter Your Address" class="large required" /><br />

				<label for="address2">Address 2:</label>
				<input type="text" name="address2" placeholder="Address Continued" title="Enter Your Address" class="large" /><br />

				<label for="city">City:</label>
				<input type="text" name="city" placeholder="City" title="Enter Your City" class="large required" /><br />

				<label for="zipcode">Zip Code:</label>
				<input type="text" name="zipcode" placeholder="Zip Code" title="Enter Your Zip Code" class="large required" /><br />

				<label for="state">State:</label>
				<select name="state" title="Select Your State" class="large required">
					<option value="" selected="selected">Please Select</option>
					<?php
						for($i = 0; $i < sizeof($states); $i++):
					?>
					<option value="<?php echo $states[$i]['ID']; ?>" <?php echo ($i == 2) ? 'selected="selected"' : ''; ?>><?php echo $states[$i]['State']; ?></option>
					<?php
						endfor;
					?>
				</select>

				<br />

				<label for="country">Country:</label>
				<select name="country" title="Select Your Country" class="large required">
					<option value="">Please Select</option>
					<?php
						for($i = 0; $i < sizeof($countries); $i++):
					?>
					<option value="<?php echo $countries[$i]['ID']; ?>" <?php echo ($countries[$i]['ID'] == 1) ? 'selected="selected"' : ''; ?>><?php echo $countries[$i]['CountryName']; ?></option>
					<?php
						endfor;
					?>
				</select>

				<br />

				<label for="phone" >Phone:</label>
				<input type="text" name="phone" placeholder="Phone Number" title="Enter Your Phone Number" class="large required"/><br />

				<label for="email">Email:</label>
				<input type="text" name="email" placeholder="Email" title="Enter Your Email" class="large required"/><br />

				<label for="tshirt">T-Shirt Size:</label>
				<select name="tshirt" class="large required" title="Enter Your T-Shirt Size">
					<option value="">Select Your T-Shirt Size</option>
					<option value="Small">Small</option>
					<option value="Medium">Medium</option>
					<option value="Large">Large</option>
					<option value="X-Large">X-Large</option>
					<option value="XX-Large">XX-Large</option>
				</select><br />

				<label for="hear_about">How'd You Hear About Us:</label>
				<select name="hear_about" class="large required" title="How did you hear about us">
					<option value="">Select How you heard about us</option>
					<option value="Champions Way">Champions Way</option>
					<option value="Direct Mail">Direct Mail</option>
					<option value="Facebook">Facebook</option>
					<option value="FC Online">FC Online Marketing Inc.</option>
					<option class="friend" value="Friend">Friend</option>
					<option value="Kru">Kru</option>
					<option value="Melody Shuman">Melody Shumann</option>
					<option value="Members Solution">Members Solution</option>
					<option class="other" value="Other">Other</option>
					<option value="Powerful Words">Powerful Words</option>
				</select><br />

				<div class="friend_input friend" style="display: none;">
					<label for="friends_name">Friends Name:</label>
					<input type="text" name="hear_friend" placeholder="Friends Name" title="Enter Your Friends Name" class="large"/><br />
				</div>

				<div class="other_input other" style="display: none;">
					<label for="other_data">Other:</label>
					<input type="text" name="hear_other" placeholder="How'd You Hear About Us?" title="Enter How you Heard About Us" class="large"/><br />
				</div>



				<?php if(intval($_GET['guest']) == 1): ?>
				<div style="width: 95%; height: 1px; margin: 25px auto; background-color: #EEEEEE;"></div>

				<label for="guest_firstname">Guests First Name:</label>
				<input type="text" name="guest_firstname" placeholder="Guest First Name" class="large required" title="Enter Guest First Name" ><br />

				<label for="guest_lastname" >Guests Last Name:</label>
				<input type="text" name="guest_lastname" class="large required" placeholder="Guest Last Name" title="Enter Guest Last Name"><br />

				<label for="guest_tshirt">Guests T-Shirt Size:</label>
				<select name="guest_tshirt" class="large required"  title="Enter Guest T-Shirt Size">
					<option value="">Select Your T-Shirt Size</option>
					<option value="Small">Small</option>
					<option value="Medium">Medium</option>
					<option value="Large">Large</option>
					<option value="X-Large">X-Large</option>
					<option value="XX-Large">XX-Large</option>
				</select><br />
				<?php endif; ?>

				<br />

				<label for="coupon">Coupon Code:</label>
				<input type="text" name="coupon" />
				<input type="button" id="btn_coupon" value="Enter Coupon" />

				<br />


				<div id="billing_wrapper">

				<!-- BILLING INFO -->
				<div class="section futura-heavy">Billing Info</div>

				<div id="billing_wrapper">

				<ul class="cards">
                	<li class="visa">Visa</li>
                	<li class="amex">American Express</li>
                	<li class="mastercard">MasterCard</li>
                	<li class="discover">Discover</li>
            	</ul>

				<label for="cc_name">Name on Card:</label>
				<input type="text" name="cc_name" class="required" title="Enter The Name on Credit Card"/><br />

				<label for="cc_num">Card Number</label>
				<input type="text" name="cc_num" class="required" title="Enter Your CC Number" /><br />

				<label for="cc_exp">Expiration Date:</label>
				<select name="cc_exp_month" class="required" >
					<option value="">Month</option>
					<?php for($i = 0; $i < 12; $i++): ?>
					<option value="<?php echo $i + 1; ?>"><?php echo $i + 1; ?></option>
					<?php endfor; ?>
				</select>
				/
				<select name="cc_exp_year" class="required" title="Enter Credit Card Expiration Date">
					<option value="">Year</option>
					<?php for($i = 0; $i < 10; $i++): ?>
					<option value="<?php echo date("Y") + $i; ?>"><?php echo date("Y") + $i; ?></option>
					<?php endfor; ?>
				</select>

				<br />

				<label for="cvv2">CVV2: <span id="cvv2_define">What is this?</span></label>
				<input type="text" name="cvv2" class="required" title="Enter Your CC CVV2"/><br />

				</div>

				</div>

				<div id="total_container">

					<table cellpadding="0" cellspacing="0">

						<tr class="summit" data-cost="<?php echo $ticket_cost; ?>">
							<td>1x MABS Ticket:<br />
							<?php if(isset($pay_option) && intval($_GET['ez']) == 3): ?>
							<?php echo $pay_option; ?>
							<?php else: ?>
							<?php echo (intval($_GET['ez']) == 3) ? '<span style="font-size: 12px;">(3 Monthly Payments of $99)</span>' : ''; ?></td>
							<?php endif; ?>
							<td>
								$<?php echo $ticket_cost; ?>
							</td>
						</tr>



						<?php if(intval($_GET['guest']) == 1): ?>
						<tr class="guest" data-cost="<?php echo $guest_cost; ?>">
							<td>1x Guest:</td>
							<td>$149</td>
						</tr>
						<?php endif; ?>

						<tr class="dvd" style="display: none;" data-cost="69">
							<td>1x DVD:</td>
							<td>$69</td>
						</tr>

						<tr class="total_coupon" style="display: none;">
							<td style="color: green;">COUPON APPLIED:</td>
							<td style="color: green;">-$<span data-cost="<?php echo (intval($_GET['guest']) == 1) ? $ticket_cost + $guest_cost : $ticket_cost; ?>"><?php echo (intval($_GET['guest']) == 1) ? $ticket_cost + $guest_cost : $ticket_cost; ?></span></td>
						</tr>

						<tr id="total">
							<td>Order Total:</td>
							<td class="order_total">$<span id="order_total" data-cost="<?php echo (intval($_GET['guest']) == 1) ? $ticket_cost + $guest_cost : $ticket_cost; ?>"><?php echo (intval($_GET['guest']) == 1) ? $ticket_cost + $guest_cost : $ticket_cost; ?></span></td>
						</tr>

					</table>

				</div>


				<input type="button" id="checkout_now" class="checkout" value="Register for MABS 14" style="float: left; margin-left: 180px;"  />
				<div style="clear: both;"></div>
				<div style="display: block; text-align: center; margin: auto; margin-top: 40px;">

					<!-- (c) 2005, 2013. Authorize.Net is a registered trademark of CyberSource Corporation -->
					<div class="AuthorizeNetSeal" style="display: inline-block; margin: auto 15px;">
						<script type="text/javascript" language="javascript">var ANS_customer_id="428d52b0-b883-4656-b268-2071069a7553";</script>
						<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script>
						<a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Online Payment Processing</a>
					</div>

					<div style="display: inline-block; margin: auto 15px;">

						<script type="text/JavaScript">
							//<![CDATA[
							var sealServer=document.location.protocol+"//seals.websiteprotection.com/sealws/51b6bce3-7adc-4cd0-a02a-445bcbdd56cb.gif";var certServer=document.location.protocol+"//certs.websiteprotection.com/sealws/?sealId=51b6bce3-7adc-4cd0-a02a-445bcbdd56cb";var hostName="atamartialartsclasses.com";document.write(unescape('<div style="text-align:center;margin:0 auto;"><a target="_blank" href="'+certServer+'&pop=true" style="display:inline-block;"><img src="'+sealServer+'" alt="Website Protection&#153; Site Scanner protects this website from security threats." title="This Website Protection site seal is issued to '+ hostName +'. Copyright &copy; 2013, all rights reserved."oncontextmenu="alert(\'Copying Prohibited by Law\'); return false;" border="0" /></a><div id="bannerLink"><a href="https://www.godaddy.com/" target="_blank">Go Daddy</a></div></div>'));
							//]]>
						</script>


					</div>

				</div>

			</form>


		</div>

		<?php include('footer.php'); ?>
	</div>
</div>
<!-- END: Page Content -->

<?php include('popup.php'); ?>


<?php include('cvv2.php'); ?>


<script src="https://www.ilovekickboxing.com/intl_js/cufon.js"></script>
<script src="js/fonts/Futura-CondensedMedium_500.font.js"></script>
<script src="js/fonts/Futura_Std_650.font.js"></script>
<script src="js/fonts/Futura_Std_Bold_Condensed.font.js"></script>
<script src="js/cufon_fonts.js"></script>

<script src="js/signup_program.js"></script>
<script src="js/abandoned_cart.js"></script>

<script src="js/jquery.creditCardValidator.js"></script>
<script src="js/demo.js"></script>


</body>
</html>