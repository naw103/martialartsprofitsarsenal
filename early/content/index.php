<?php
header('Location: https://www.martialartsprofitsarsenal.com/');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

      <title>5 Day of FREE Content from Mike Parrella</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="video-js/video-js.css?ver=1.0" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="video-js/video.js"></script>
</head>

<body>

    <!-- Navigation -->
    

    <!-- Header -->
    <div class="intro-header">

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <!--<h3 class="w">Brought to you by <a href="http://profitsarsenal.com">Martial Arts Profits Arsenal</a>. The New School-Growing Program from Team Parrella.</h3>-->
                        
                        <h1><span class="r">Your content!<br />From Team Parrella</span></h1>

                            <hr>

						<?php if($now >= $monday || $show_all): ?>
                        <h3 class="w"><span class="day">Monday:</span> The 3 Emails Every Prospect Should Get to Convert Up to 50% More Into Trials... On Auto-Pilot.</h3>


						<i class="fa fa-file-text fa-5x" ></i>

						     

                            <h3 class="text-center"><a href="docs/email1.pdf">Click now to open!</a></h3>
                            
                            <hr>

							<?php endif; ?>

							<?php if($now >= $tuesday || $show_all): ?>
                             <h3 class="w"><span class="day">Tuesday:</span> Our 4-Step Method for Getting Youtube Videos to Dominate Page 1 of Google, Leading to More Traffic, and More Sales.
</h3>
                            <div class="video_container">

									<video data-setup='{"example_option":true}' controls="controls" class="video-js vjs-default-skin vjs-big-play-centered" poster=""  >
						         		<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/adan.mp4" type="video/mp4" />
										<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/adan.webm" type="video/webm" />
										<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/adan.ogv" type="video/ogg" />

										<object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
											<param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
											<param name="allowFullScreen" value="true" />
											<param name="wmode" value="transparent" />
											<param name="flashVars" value="config={'playlist':[{'url':'http%3A%2F%2Fwww.ilovekickboxing.com%2Fsvn%2Fvideos%2Fconverted-web%2Fmaba_webinar.mp4','autoPlay':false}]}" />
											<span title="No video playback capabilities, please download the video below">Video 1</span>
										</object>
									</video>
						            </div>
                           <!--<h3 class="text-center"><a href="#">PDF with email!</a></h3>-->
                            
                            <hr>
							<?php endif; ?>

							<?php if($now >= $wednesday || $show_all): ?>
                             <h3 class="w"><span class="day">Wednesday:</span> Instantly generate 15% more leads per month with this fast, EASY, powerful “Facebook Group Method”</h3>
							<i class="fa fa-file-text fa-5x" ></i>

                            <h3 class="text-center"><a href="docs/3-FB-Groups.pdf">Click now to open!</a></h3>

                            <hr>
							<?php endif; ?>

							<?php if($now >= $thursday || $show_all): ?>
                             <h3 class="w"><span class="day">Thursday:</span> 5 Secrets of the Staff Hiring Process for Instructors Who Increase Retention, Referrals, and Enrollments Like Crazy (Schools have literally transformed from this strategy alone)</h3>
                                                   <div class="video_container">

									<video data-setup='{"example_option":true}' controls="controls" class="video-js vjs-default-skin vjs-big-play-centered" poster=""  >
						         		<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/kelly.mp4" type="video/mp4" />
										<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/kelly.webm" type="video/webm" />
										<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/kelly.ogv" type="video/ogg" />

										<object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
											<param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
											<param name="allowFullScreen" value="true" />
											<param name="wmode" value="transparent" />
											<param name="flashVars" value="config={'playlist':[{'url':'http%3A%2F%2Fwww.ilovekickboxing.com%2Fsvn%2Fvideos%2Fconverted-web%2Fmaba_webinar.mp4','autoPlay':false}]}" />
											<span title="No video playback capabilities, please download the video below">Video 2</span>
										</object>
									</video>
						            </div>
                            <!--<h3 class="text-center"><a href="#">PDF with email!</a></h3>-->
                            
                            <hr>
							<?php endif; ?>

							<?php if($now >= $friday || $show_all): ?>
                             <h3 class="w"><span class="day">Friday:</span> Convert 30% more of your trials into students TODAY with this "under the radar" enrollment strategy (Hint: It's NOT sleazy, salesy, or high-pressured at all. It's natural, simple, and friendly.)</h3>
                                               <div class="video_container">

									<video data-setup='{"example_option":true}' controls="controls" class="video-js vjs-default-skin vjs-big-play-centered" poster=""  >
						         		<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/5-enrollments.mp4" type="video/mp4" />
										<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/5-enrollments.webm" type="video/webm" />
										<source src="https://www.martialartsprofitsarsenal.com/early/content/docs/5-enrollments.ogv" type="video/ogg" />

										<object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
											<param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
											<param name="allowFullScreen" value="true" />
											<param name="wmode" value="transparent" />
											<param name="flashVars" value="config={'playlist':[{'url':'http%3A%2F%2Fwww.ilovekickboxing.com%2Fsvn%2Fvideos%2Fconverted-web%2Fmaba_webinar.mp4','autoPlay':false}]}" />
											<span title="No video playback capabilities, please download the video below">Video 3</span>
										</object>
									</video>
						            </div>
                            <!--<h3 class="text-center"><a href="#">PDF with email!</a></h3>-->
                       		<?php endif; ?>

                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

    

    <!-- Footer -->
    

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
