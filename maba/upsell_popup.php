<!-- BEGIN: Pop Up -->
<table id="upsell_container" class="popup">
	<tr>
		<td>
			<div class="left_side">

				<div class="headline futura-condensed">
					Register Now!<br /><br />

				</div>

				<div class="white-border"></div>

				<img src="images/guarantee.png" width="200" height="200" alt="2x Money Back Guarantee" />

				<p class="guarantee">
					If you decide during MABS that the info is not cutting
					edge and will not help you with your business then let
					our support staff know immediately and we'll give you
					a 200% refund on the spot.
				</p>

			</div>
		</td>

		<td>
			<div class="right_side">

				<div class="close">X</div>


				<div class="section futura-condensed" style="text-indent: 30px; font-size: 24px; text-transform: uppercase; margin-bottom: 17px; padding-top: 30px;">1. Do you want to preorder a MABS 2015 DVD?</div>

                <img src="images/dvd.jpg" alt=""  style="float: right; margin-right: 0px; margin-left: -10px;"/>

				<select name="upsell_dvd" style="margin-top: 45px; text-indent: 4px; width: 262px;">
					<option value="0">No Thanks, no DVD</option>
					<option value="1">Yes, I want to Pre-Order a DVD</option>
				</select>

                <div class="clear"></div>

                <div id="upsell_total" class="total_container">
	<table cellpadding="0" cellspacing="0">

                    <?php if($ticket && $pay_in_full): ?>
					<tr class="ticket" data-payinfull="<?php echo $pay; ?>"  data-ez="" data-cost="<?php echo $ticket_info['cost']; ?>" data-realcost="<?php echo $actual_ticket_cost; ?>">
						<td><?php echo $ticket_info['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo $ticket_info['cost']; ?></span></td>
					</tr>
                    <?php endif; ?>

                    <?php if($ticket && !$pay_in_full): ?>
					<tr class="ticket" data-payinfull="<?php echo $pay; ?>" data-ez="<?php echo $ticket_info['ez_id']; ?>" data-cost="<?php echo $ticket_info['ez_cost']; ?>" data-eznum="<?php echo $ticket_info['ez_num']; ?>" data-realcost="<?php echo $actual_ticket_cost; ?>">
						<td><?php echo $ticket_info['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo $ticket_info['ez_cost']; ?></span></td>
					</tr>
                    <!--
                    <tr class="ticket_ez">
                        <td colspan="2">
                            (<?php echo $ticket_info['ez_num']; ?> Payments of <span>$<?php echo $ticket_info['ez_cost']; ?></span>)
                        </td>
                    </tr>
                    -->
                    <?php endif; ?>


                    <?php if($guest): ?>
					<tr class="guest" data-cost="<?php echo $guest_info['cost']; ?>" data-realcost="<?php echo $actual_guest_cost; ?>">
						<td><?php echo $guest_info['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo $guest_info['cost']; ?></span></td>
					</tr>
                    <?php if(1 == 0): ?>
                    <tr class="ticket_ez">
                        <td colspan="2">
                            (<?php echo $ticket_info['ez_num']; ?> Payments of <span>$<?php echo $guest_info['cost']; ?></span>)
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endif; ?>


                    <?php if($dvd): ?>
					<tr class="dvd" data-cost="<?php echo $dvd_info['cost']; ?>" data-realcost="<?php echo $actual_dvd_cost; ?>">
						<td><?php echo $dvd_info['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo $dvd_info['cost']; ?></span></td>
					</tr>
                    <?php endif; ?>

                    <tr class="coupon_discount hidden">
                         <td>Coupon Discount:</td>
                         <td></td>
                     </tr>

                     <tr class="coupon_amt hidden">
                         <td>Coupon Amount:</td>
                         <td></td>
                     </tr>

                     <?php if(!$pay_in_full): ?>
					 <tr class="order_total payments" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="">
                        <td>
                            <?php echo $ticket_info['ez_num']; ?> payments of:

                        </td>
						<td>

                        $<span class="cost"><?php echo $start_total; ?></span></td>
					</tr>
                    <?php endif; ?>


					<tr <?php echo ($pay_in_full) ? 'class="order_total"' : 'class="actual_order_total"'; ?> data-cost="<?php echo $total_purchase; ?>" data-couponprod="" data-couponamt="" data-coupontype="">
						<td>
                            Order Total:
                        </td>
                        <td class="cost">
                            $<span class="cost"><?php echo $total_purchase; ?></span>
                        </td>
                    </tr>
                     <?php if(!$pay_in_full): ?>
					 <tr class="order_total" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="">
                        <td>
                            Total paid today:

                        </td>
						<td>

                        $<span class="cost"><?php echo $start_total; ?></span></td>
					</tr>
                    <?php endif; ?>
				</table>

			</div>


				<input type="button" id="checkout" class="checkout" value="Register" style="float: none; display: block; margin: auto; margin-bottom: 20px;  margin-top: 40px;"/>


			</div>
		</td>
	</tr>
</table>
<!-- END: Pop Up -->