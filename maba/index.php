<?php
 //mail('james@fconlinemarketing.com', 'mapa top', 'test');
	require('program/program.php');
	require('program/definitions.php');
	require('program/class.checkout.php');
	require('program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

    $all_orders = $db->all_orders($site_id);
   
    if(sizeof($all_orders) >= 255):
        $db->price_change();
    endif;

	$mapa = $db->get_product_info($mapa_id, fALSE);

	$states = $db->get_all_states();
	$countries = $db->get_all_countries();

	$mapa_cost = $mapa['ez_pay'][0]['cost'];
	$ezpay_id = $mapa['ez_pay'][0]['id'];
	$ezpay_num = $mapa['ez_pay'][0]['num'];
	$ezpay_interval = $mapa['ez_pay'][0]['interval'];
	$total_purchased = $mapa_cost;
	$start_total = $mapa_cost;


	$coupon = $db->active_coupon($site_id);

	$db->close();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Profits Arsenal by Mike Parrella</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">

    <link href="../css/ekko-lightbox.css" rel="stylesheet">
	<link href="../css/main.css" rel="stylesheet">

	<!--[if lt IE 9]>
	  <script src="../js/html5shiv.js"></script>
	  <script src="../js/respond.min.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="../images/favicon.png">
	<link rel="stylesheet" href="../css/simple-line-icons.css"/>
	<link rel="stylesheet" href="css/styles.css"/>
<link rel="stylesheet" href="css/signup.css" />

<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>



</head>

<body>



    <div class="intro-header" style="background: none;">

        <div class="container">

    <?php if($open_checkout): ?>
		<!-- BEGIN: Page Content -->
	<div id="page_content">

		<div id="headline_container">

            <h1 class="futura"><i class="fa fa-bolt fa-lg"></i> <?php echo $mapa['product_name']; ?></h1>
<!--
			<p style="font-size: 22px; color: red; margin: 20px auto 00px auto; text-align: center; font-weight: bold;">
				First payment will be on April 1st.
			</p>
-->
			<p style="font-size: 22px; color: #383838; margin: 20px 20px 20px 80px;">
				Fill in the information below to get started now!
			</p>

        </div>


		<div id="form_wrapper">

			<!-- CONTACT INFO -->
			<div id="contact_wrapper">
				<div class="section futura">Contact Info</div>


				<label for="firstname">First Name:</label>
				<input type="text" name="firstname" placeholder="First Name" title="Enter Your First Name" class="large required" /><br />

				<label for="lastname">Last Name:</label>
				<input type="text" name="lastname" placeholder="Last Name" title="Enter Your Last Name" class="large required" /><br />

				<label for="address">Address:</label>
				<input type="text" name="address" placeholder=" Address" title="Enter Your Address" class="large required" /><br />

				<label for="address2">Address 2:</label>
				<input type="text" name="address2" placeholder="Address Continued" title="Enter Your Address" class="large" /><br />

				<label for="city">City:</label>
				<input type="text" name="city" placeholder="City" title="Enter Your City" class="large required" /><br />

				<label for="zipcode">Zip Code:</label>
				<input type="text" name="zipcode" placeholder="Zip Code" title="Enter Your Zip Code" class="large required" /><br />

				<label for="state">State:</label>
				<select name="state" title="Select Your State" class="large required">
					<option value="" selected="selected">Please Select</option>
					<?php
						for($i = 0; $i < sizeof($states); $i++):
					?>
					<option value="<?php echo $states[$i]['ID']; ?>" <?php echo ($i == 2) ? 'selected="selected"' : ''; ?>><?php echo $states[$i]['State']; ?></option>
					<?php
						endfor;
					?>
				</select>

				<br />

				<label for="country">Country:</label>
				<select name="country" title="Select Your Country" class="large required">
					<option value="">Please Select</option>
					<?php
						for($i = 0; $i < sizeof($countries); $i++):
					?>
					<option value="<?php echo $countries[$i]['ID']; ?>" <?php echo ($countries[$i]['ID'] == 1) ? 'selected="selected"' : ''; ?>><?php echo $countries[$i]['CountryName']; ?></option>
					<?php
						endfor;
					?>
				</select>

				<br />

				<label for="phone" >Phone:</label>
				<input type="text" name="phone" placeholder="Phone Number" title="Enter Your Phone Number" class="large required"/><br />

				<label for="email">Email:</label>
				<input type="text" name="email" placeholder="Email" title="Enter Your Email" class="large required"/><br />



                <label for="hear_about">Hear About:</label>
                <select name="hear_about" class="large required">
                    <option value="">Please Select</option>

					<option value="Facebook">Facebook</option>
					<option value="Email Promotion">Email Promotion</option>
                    <option value="From a Friend">From a Friend</option>
					<option value="Other">Other</option>
                </select>

				<div class="friend hidden">
					<label for="hear_about">Friend:</label>
					<input type="text" name="hear_about_friend" class="required" /><br />
				</div>

				<div class="other hidden">
					<label for="hear_about">Other:</label>
					<input type="text" name="hear_about_other" class="required" /><br />
				</div>




				<br />
				<br />

                <?php if($coupon && !$dollardown): ?>
				<label for="coupon">Coupon:</label>
                <input type="text" name="coupon" />
                <input type="button" id="btn_coupon" value="Submit Coupon" />
				<br />
                <?php endif; ?>
			</div>

			<!-- BILLING INFO -->
			<div id="billing_wrapper">

				<div class="section futura">Billing Info</div>

				<div id="">


				<ul class="cards">
                	<li class="visa">Visa</li>
                	<li class="amex">American Express</li>
                	<li class="mastercard">MasterCard</li>
                	<li class="discover">Discover</li>
            	</ul>

				<label for="cc_name">Name on Card:</label>
				<input type="text" name="cc_name" class="required" title="Enter The Name on Credit Card" value="<?php echo stripslashes($customer['firstname']); ?> <?php echo stripslashes($customer['lastname']); ?>"/><br />

				<label for="cc_num">Card Number</label>
				<input type="text" name="cc_num" class="required" title="Enter Your CC Number" /><br />

				<label for="cc_exp">Expiration Date:</label>
				<select name="cc_exp_month" class="required" >
					<option value="">Month</option>
					<?php for($i = 0; $i < 12; $i++): ?>
					<option value="<?php echo $i + 1; ?>"><?php echo $i + 1; ?></option>
					<?php endfor; ?>
				</select>
				/
				<select name="cc_exp_year" class="required" title="Enter Credit Card Expiration Date">
					<option value="">Year</option>
					<?php for($i = 0; $i < 10; $i++): ?>
					<option value="<?php echo date("Y") + $i; ?>"><?php echo date("Y") + $i; ?></option>
					<?php endfor; ?>
				</select>

				<br />

				<label for="cvv2">CVV2: <!--<span id="cvv2_define">What is this?</span>--></label>
				<input type="text" name="cvv2" class="required" title="Enter Your CC CVV2"/><br />

				</div>

			</div>

			<?php if($dollardown): ?>
			<div id="main_total" class="total_container" style="float: none; margin-left: 201px; margin-top: 25px;">
				<table cellpadding="0" cellspacing="0">


					<tr class="ticket" data-payinfull="<?php echo $pay_in_full; ?>" data-ez="<?php echo $ezpay_id; ?>" data-cost="1" data-eznum="<?php echo $ezpay_num; ?>" data-realcost="<?php echo $mapa_cost; ?>">
						<td><?php echo $mapa['abbr_name']; ?>:</td>
						<td>$1.00 Down</td>
					</tr>

					<tr>
						<td>Recurring</td>
						<td>$<span class="cost"><?php echo number_format($mapa_cost, 2); ?></span>/month</td>
					</tr>



                    <tr class="coupon_discount" style="display: none;">
                         <td>Coupon Discount:</td>
                         <td></td>
                     </tr>

                     <tr class="coupon_amt" style="display: none;">
                         <td>Coupon Amount:</td>
                         <td></td>
                     </tr>


					 <tr class="order_total payments" style="display: none;" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="" style="background: none; color: #333333;">
                        <td colspan="2">

                        $<span class="cost"><?php echo $mapa_cost; ?></span>)/<?php echo $ezpay_interval; ?></td>
					</tr>




					<tr <?php echo ($pay_in_full) ? 'class="order_total"' : 'class="actual_order_total"'; ?> data-cost="1" data-couponprod="" data-couponamt="" data-coupontype="" style="background: #AAAAAA;">
						<td>
                            Order Total:
                        </td>
                        <td class="cost">
                            $<span class="cost">1.00</span>
                        </td>
                    </tr>
                     <?php if(!$pay_in_full): ?>
					 <tr class="order_total" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="">
                        <td>
                            Total paid today:

                        </td>
						<td>

                        $<span class="cost"><?php echo $start_total; ?></span></td>
					</tr>
                    <?php endif; ?>
				</table>

			</div>
			<?php else: ?>
			<div id="main_total" class="total_container" style="float: none; margin-left: 201px; margin-top: 25px;">
				<table cellpadding="0" cellspacing="0">


					<tr class="ticket" data-payinfull="<?php echo $pay_in_full; ?>" data-ez="<?php echo $ezpay_id; ?>" data-cost="<?php echo $mapa_cost; ?>" data-eznum="<?php echo $ezpay_num; ?>" data-realcost="<?php echo $mapa_cost; ?>">
						<td><?php echo $mapa['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo number_format($mapa_cost, 2); ?></span>/month</td>
					</tr>



                    <tr class="coupon_discount" style="display: none;">
                         <td>Coupon Discount:</td>
                         <td></td>
                     </tr>

                     <tr class="coupon_amt" style="display: none;">
                         <td>Coupon Amount:</td>
                         <td></td>
                     </tr>


					 <tr class="order_total payments" style="display: none;" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="" style="background: none; color: #333333;">
                        <td colspan="2">

                        $<span class="cost"><?php echo $mapa_cost; ?></span>)/<?php echo $ezpay_interval; ?></td>
					</tr>




					<tr <?php echo ($pay_in_full) ? 'class="order_total"' : 'class="actual_order_total"'; ?> data-cost="<?php echo $total_purchase; ?>" data-couponprod="" data-couponamt="" data-coupontype="" style="background: #AAAAAA;">
						<td>
                            Order Total:
                        </td>
                        <td class="cost">
                            $<span class="cost"><?php echo $total_purchase; ?></span>
                        </td>
                    </tr>
                     <?php if(!$pay_in_full): ?>
					 <tr class="order_total" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="">
                        <td>
                            Total paid today:

                        </td>
						<td>

                        $<span class="cost"><?php echo $start_total; ?></span></td>
					</tr>
                    <?php endif; ?>
				</table>

			</div>
			<?php endif; ?>
			<div style="color: #383838; padding-top: 7px; font-size: 13px; text-align: center; width: 400px; margin: auto; margin-left: 135px;">There is a 30 Day Cancellation Policy from the day you submit
			your request to cancel to <a href="info@profitsarsenal.com" style="color: #383838;">info@profitsarsenal.com</a></div>

			<button id="checkout"class="btn btn-warning btn-lg" style="margin-left: 132px; margin-top: 35px;">Count me in!
                </button>



							<div style="display: block; text-align: center; margin: auto; margin-top: 80px;">

					<!-- (c) 2005, 2013. Authorize.Net is a registered trademark of CyberSource Corporation -->
					<div class="AuthorizeNetSeal" style="display: inline-block; margin: auto 15px;">
						<script type="text/javascript" language="javascript">var ANS_customer_id="428d52b0-b883-4656-b268-2071069a7553";</script>
						<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script>
						<a href="https://www.authorize.net/" id="AuthorizeNetText" target="_blank">Online Payment Processing</a>
					</div>
				<!--
					<div style="display: inline-block; margin: auto 15px;">

						<script type="text/JavaScript">
							//<![CDATA[
							var sealServer=document.location.protocol+"//seals.websiteprotection.com/sealws/51b6bce3-7adc-4cd0-a02a-445bcbdd56cb.gif";var certServer=document.location.protocol+"//certs.websiteprotection.com/sealws/?sealId=51b6bce3-7adc-4cd0-a02a-445bcbdd56cb";var hostName="atamartialartsclasses.com";document.write(unescape('<div style="text-align:center;margin:0 auto;"><a target="_blank" href="'+certServer+'&pop=true" style="display:inline-block;"><img src="'+sealServer+'" alt="Website Protection&#153; Site Scanner protects this website from security threats." title="This Website Protection site seal is issued to '+ hostName +'. Copyright &copy; 2013, all rights reserved."oncontextmenu="alert(\'Copying Prohibited by Law\'); return false;" border="0" /></a><div id="bannerLink"><a href="https://www.godaddy.com/" target="_blank">Go Daddy</a></div></div>'));
							//]]>
						</script>


					</div>
					-->
				</div>


		</div>

		</div>
            <?php else: ?>
            <h4 style="font-size: 26px; color: #000; padding-bottom: 40px;">Registration is now closed.</h4>

            <?php endif; ?>
        </div>


    </div>
    <!-- /.container -->






    <footer>
      <div class="container">
			<section class="row breath">
				<div class="col-md-12 footerlinks">
					<p>&copy; 2014 Parrella Consulting Inc. All Rights Reserved</p>
				</div>
			</section><!--/section -->
		</div>
    </footer>

<?php if($open_checkout): ?>
<div id="overlay" <?php echo $show ? 'class="show"' : ''; ?>></div>
<?php include('popup.php'); ?>

<div id="loader">

    <img src="images/loader.gif" alt="Processing" />
	<div>Processing Payment...</div>
	<div style="color: red; font-weight: none; font-size: 15px; margin-top: 7px;">
		Do Not Leave This Page While Order is Processing.<br />
		(May Take Up to 2 Minutes)
	</div>

</div>

<div id="alert_overlay"></div>
<div id="alert_container">
    <div id="alert_copy">

    </div>
    <input type="button" id="alert_ok" value="OK" />
</div>
<?php endif; ?>

    <script src="../js/bootstrap.js"></script>
	<script src="js/jquery.creditCardValidator.js"></script>
<script src="js/demo.js"></script>
<script src="js/process_order.js"></script>
</body>

</html>
