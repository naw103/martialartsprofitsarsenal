<?php
	#header('Location: https://www.martialartsprofitsarsenal.com/5days/index.php');

	date_default_timezone_set('America/New_York');

	$now = date('Y-m-d');

	$monday = date('2014-12-22');
	$tuesday = date('2014-12-23');
	$wednesday = date('2014-12-24');
	$thursday = date('2014-12-25');
	$friday = date('2014-12-26');

	$show_all = FALSE;
	if($_GET['show'] == 'DAVID'):
		$show_all = TRUE;
	endif;




?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

      <title>5 MORE days of FREE School-Growing Content!</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="video-js/video-js.css?ver=1.0" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="video-js/video.js"></script>
</head>

<body>

    <!-- Navigation -->
    

    <!-- Header -->
    <div class="intro-header">

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <!--<h3 class="w">Brought to you by <a href="http://profitsarsenal.com">Martial Arts Profits Arsenal</a>. The New School-Growing Program from Team Parrella.</h3>-->
                        
                        <h1><span class="r">Your content!<br />From Team Parrella &amp; Rainmaker</span></h1>

                            <hr>

						<?php if($now >= $monday || $show_all): ?>
                        <h3 class="w"><span class="day">Monday:</span> Start Selling Out Your Events. Just follow this proven step-by-step plan.</h3>


						<i class="fa fa-file-text fa-5x" ></i>



                            <h3 class="text-center"><a href="docs/day1.pdf">Click now to open!</a></h3>

                            <hr>

						<?php endif; ?>

						<?php if($now >= $tuesday || $show_all): ?>
                            <h3 class="w"><span class="day">Tuesday:</span> Convert Your Staff Into Money Makers. Just follow this proven step-by-step plan.</h3>


	    					<i class="fa fa-file-text fa-5x" ></i>



                            <h3 class="text-center"><a href="docs/day2.pdf">Click now to open!</a></h3>

                            <hr>

						<?php endif; ?>

						<?php if($now >= $wednesday || $show_all): ?>
                            <h3 class="w"><span class="day">Wednesday:</span> Set Your Email Marketing on FIRE and Make it 500% More Effective With List Segmenting (This guide makes it EASY)</h3>


	    					<i class="fa fa-file-text fa-5x" ></i>



                            <h3 class="text-center"><a href="docs/day3.pdf">Click now to open!</a></h3>

                            <hr>

						<?php endif; ?>

						<?php if($now >= $thursday || $show_all): ?>
                            <h3 class="w"><span class="day">Thursday:</span> Boost Your Intro Show Rate by 85% and Enroll More Members.</h3>


	    					<i class="fa fa-file-text fa-5x" ></i>



                            <h3 class="text-center"><a href="docs/day4.pdf">Click now to open!</a></h3>

                            <hr>

						<?php endif; ?>

						<?php if($now >= $friday || $show_all): ?>
                            <h3 class="w"><span class="day">Friday:</span> The Facebook Cheat Sheet: Getting Paid Trials Today Through Facebook</h3>


	    					<i class="fa fa-file-text fa-5x" ></i>



                            <h3 class="text-center"><a href="docs/day5.pdf">Click now to open!</a></h3>

                            <hr>

						<?php endif; ?>


                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

    

    <!-- Footer -->
    

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
