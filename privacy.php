<?php



	require('checkout/program/program.php');
	require('checkout/program/definitions.php');
	require('checkout/program/class.checkout.php');
	require('checkout/program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

     $all_orders = $db->all_orders($site_id);
	$class = '';
    if(sizeof($all_orders) >= 255):
        $db->price_change();
		$class = 'above_250';
    endif;

	$mapa = $db->get_product_info($mapa_id, fALSE);

	$states = $db->get_all_states();
	$countries = $db->get_all_countries();

	$mapa_cost = (int)$mapa['ez_pay'][0]['cost'];
	$ezpay_id = $mapa['ez_pay'][0]['id'];
	$ezpay_num = $mapa['ez_pay'][0]['num'];
	$ezpay_interval = $mapa['ez_pay'][0]['interval'];
	$total_purchased = $mapa_cost;
	$start_total = $mapa_cost;



	$db->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<title>Martial Arts Profits Arsenal by Mike Parrella</title>

	<meta property="og:title" content="Martial Arts Profits Arsenal by Mike Parrella" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="https://www.martialartsprofitsarsenal.com/images/mapa.jpg" />
	<meta property="og:url" content="https://www.martialartsprofitsarsenal.com/" />
	<meta property="og:description" content='MA Profits Arsenal: A "Done for You" Stash of Incredible Marketing Content to Grow Your School, FAST. Now Live! Click now to learn more and get started today!' />
	<meta property="og:site_name" content="martialartsprofitsarsenal.com/" />

	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/ekko-lightbox.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="early/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	  <script src="js/html5shiv.js"></script>
	  <script src="js/respond.min.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.png">
	<link rel="stylesheet" href="css/simple-line-icons.css"/>
	<link rel="stylesheet" href="css/responsive.css"/>

</head>

<body class="<?php echo $class; ?>">
	<div id="preloader">
		<div id="status"></div>
	</div>
    <!-- ******************** MASTHEAD SECTION ******************** -->

	<!-- STICKY NAVIGATION -->
	<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
		<div class="container">
			<div class="navbar-header">

				<!-- LOGO ON STICKY NAV BAR -->
				<button type="button" class="navbar-toggle" data-toggle="" data-target="#kane-navigation">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#"><img src="images/bullet.png" alt=""></a>

			</div>

			<!-- NAVIGATION LINKS -->
			<div class="navbar-collapse collapse" id="kane-navigation">
				<ul class="nav navbar-nav navbar-right main-navigation">
					<!--<li><a href="#home">Home</a></li>-->
					<li><a href="#explore">What it is</a></li>
					<li><a href="#what_you_get">What you get</a></li>
					<li><a href="#reviews">Rave Reviews</a></li>
					<li><a href="#faq">FAQ</a></li>
					<!--<li><a href="#location">Sticky Header</a></li>-->
                    <li><a href="#prices">Prices &amp; Guarantee</a></li>
					<li class="tickets"><a href="checkout/index.php">Get Started!</a></li>
				</ul>
			</div>
		</div> <!-- /END CONTAINER -->
	</div> <!-- /END STICKY NAVIGATION -->

	<main id="top" class="masthead" role="main">
<div id="top_part">
		<div id="top_part_container">

        </div>
</div>
		<div class="container">

		<p style="text-align: center; padding-top: 80px;"><strong>PRIVACY POLICY:</strong></p>

		<p>
			We respect and are committed to protecting your privacy. We may collect
			personally identifiable information when you visit our site. We also automatically
			receive and record information on our server logs from your browser including
			your IP address, cookie information and the page(s) you visited. We will not sell
			your personally identifiable information to anyone. (And so on...)
		</p>

		<p><strong>SECURITY POLICY:</strong></p>

		<p>
			Your payment and personal information is always safe. Our Secure Sockets
Layer (SSL) software is the industry standard and among the best software
available today for secure commerce transactions. It encrypts all of your personal
information, including credit card number, name, and address, so that it cannot
be read over the internet. (Etc.)
		</p>

		<p><strong>REFUND POLICY:</strong></p>

		<p>
All refunds will be provided as a credit to the credit card used at the time of
purchase within five (5) business days upon receipt of the returned
merchandise.
		</p>

			<p><strong>Shipping Policy/Delivery Policy:</strong></p>

		<p>
Please be assured that your items will ship out within two days of purchase.
We determine the most efficient shipping carrier for your order. The carriers that
may be used are: U.S. Postal Service (USPS), United Parcel Service (UPS) or
FedEx. Sorry but we cannot ship to P.O. Boxes.
<br /><br />
If you're trying to estimate when a package will be delivered, please note the
following:
Credit card authorization and verification must be received prior to processing.
Federal Express and UPS deliveries occur Monday through Friday, excluding
holidays.
If you require express or 2 day shipping, please call us at 303.477.3361 for
charges.
		</p>

</div>

</main>


	    <!-- ******************** FOOTER ******************** -->


		<div class="container">
			<section class="row breath">
				<div class="col-md-12 footerlinks">
				<p>
			<a href="index.php">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			<a href="privacy.php">Privacy Policy</a>
		</p>
					<p>&copy; 2014 Parrella Consulting Inc. All Rights Reserved</p>
				</div>
			</section><!--/section -->
		</div><!--/container -->


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
        <h3 style="text-transform:none !important;font-weight:bold !important;">Coming soon! Officially opens on Monday, October 6, 2014!</h3>
          <h4 style="font-weight:normal !important;text-transform:none !important;"><a href="http://profitsarsenal.com/early/index.html">Click here</a> for some FREE school-growing videos while you wait!</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




 <!--/javascript -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/easing.js"></script>
<!--<script src="js/nicescroll.js"></script>-->
<script src="js/lightbox.min.js"></script>
<script src="js/jquery.nav.js"></script>

<script>
 $(window).load(function() {
	//Preloader
	$('#status').delay(300).fadeOut();
	$('#preloader').delay(300).fadeOut('slow');
    //$(".navbar").css('height','0px');
	$('body').delay(550).css({'overflow':'visible'});
    $('#top_part').delay(550).fadeIn('500');
})
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

 $(function() {
    $('.scrollto, .gototop').bind('click',function(event){
		 var $anchor = $(this);
		 $('html, body').stop().animate({
         scrollTop: $($anchor.attr('href')).offset().top
          }, 1500,'easeInOutExpo');
     event.preventDefault();
      });
  });
  $(window).load(function(){
    $('.preloader').fadeOut(3000); // set duration in brackets

});

</script>

    <script>
    // set the date we're counting down to
    /*
var target_date = new Date('Jan, 31, 2014').getTime();

// variables for time units
var days, hours, minutes, seconds;

// get tag element
var countdown = document.getElementById('countdown');

// update the tag with id "countdown" every 1 second
setInterval(function () {

    // find the amount of "seconds" between now and target
    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;

    // do some time calculations
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    // format countdown string + set tag value
    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
    + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>';

}, 1000);
*/
</script>


<script>


var elem = $('#faq .panel');

elem.find('.panel-heading').each(function(){



});

</script>

<script>
    $(document).ready(function(){

        $('#close_top').click(function(){
           $('#top').css('margin-top', '0px');
           $('#top_part').fadeOut(600);
        });

$(".navbar-toggle").click(function(){
    var element = $(this).attr('data-target');
    if($(element).hasClass('collapse')){
        $(element).removeClass('collapse');
    }
    else
    {
        $(element).addClass('collapse');
    }
});


/* =================================
===  STICKY NAV                 ====
=================================== */

$(document).ready(function() {
  $('.main-navigation').onePageNav({
    scrollThreshold: 0.2, // Adjust if Navigation highlights too early or too late
    filter: ':not(.external)',
    changeHash: true
  });

});


/* COLLAPSE NAVIGATION ON MOBILE AFTER CLICKING ON LINK - ADDED ON V1.5*/

if (matchMedia('(max-width: 480px)').matches) {
    $('.main-navigation a').on('click', function () {
        $(".navbar-toggle").click();
    });
}


/* NAVIGATION VISIBLE ON SCROLL */

$(document).ready(function () {
    mainNav();
});

$(window).scroll(function () {
    mainNav();
});

if (matchMedia('(min-width: 992px), (max-width: 767px)').matches) {
  function mainNav() {
        var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        if (top > 40) $('.sticky-navigation').stop().animate({"top": '0'});

        else $('.sticky-navigation').stop().animate({"top": '-60'});
    }
}

if (matchMedia('(min-width: 768px) and (max-width: 991px)').matches) {
  function mainNav() {
        var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        if (top > 40) $('.sticky-navigation').stop().animate({"top": '0'});

        else $('.sticky-navigation').stop().animate({"top": '0'});
    }
}


    });
</script>
<?php include("/home/content/65/11672465/html/adroll.php"); ?>
</body>
</html>